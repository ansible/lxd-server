---

- debug:
    var: lxd_containers
  when: lxd_testing | bool

- name: Pausing for {{ lxd_testing_timeout }} seconds
  ansible.builtin.wait_for:
    timeout: "{{ lxd_testing_timeout }}"
  when: lxd_testing | bool

# set default values here, and override as needed when defining each container
# https://docs.ansible.com/ansible/latest/collections/community/general/lxd_container_module.html
- name: "Deploy our LXD containers on Ubuntu {{ ansible_distribution_release }}"
  community.general.lxd_container:
    # the unix domain socket path or the https URL for the LXD server
    url: "{{ container.url | default(lxd.socket) }}"
    name: "{{ container.name }}"
    state: "{{ container.status }}"
    # either "container" or "virtual-machine" (defaults to container if field is absent)
    # by specifying it, we give ourselves the option of creating virtual machines
    type: "{{ container.type | default('container') }}"
    source:
      type: image
      # not sure what other modes are available
      mode: "{{ container.mode | default('pull') }}"
      # there are other image servers, e.g., https://images.linuxcontainers.org
      server: "{{ container.server | default('https://cloud-images.ubuntu.com/releases') }}"
      # protocol accepts either "lxd" or "simplestreams"
      # LXD supports importing image from remote image server using either LXD/simplestreams protocol
      # https://linuxcontainers.org/lxd/docs/master/image-handling/#remote-image-server-lxd-or-simplestreams
      # LXD protocol is a TLS based protocol, and simplestreams protocol is a HTTP based protocol
      protocol: "{{ container.protocol | default ('simplestreams') }}"
      # Images are identified by their hash, or more commonly their alias
      # `lxc remote list` to see configured remotes (ubuntu.com, linuxcontainers.org, etc.)
      # list all images from ubuntu remote: `lxc image list ubuntu:`, or list all
      # images from the linuxcontainers remote: `lxc image list images:`.
      # Similarly, to list only all image aliases: `lxc image alias list ubuntu:`, or
      # `lxc image alias list images:`.
      # https://ubuntu.com/server/docs/containers-lxd
      # https://ubuntu.com/blog/lxd-2-0-your-first-lxd-container
      # https://ubuntu.com/blog/lxd-2-0-image-management-512
      alias: "{{ container.alias }}"
      # architecture, e.g., x86_64 or i686
      architecture: "{{ container.architecture | default('x86_64') }}"
    profiles: ["{{ container.profile | default ('default') }}"]
    # this keep the old "volatile options" behaviour (whatever good that did us is unclear)
    # https://docs.ansible.com/ansible/latest/collections/community/general/lxd_container_module.html#parameter-ignore_volatile_options
    ignore_volatile_options: true
    config:
      security.privileged: "{{ container.security_privileged | default ('false') }}"
      # Docker container inside LXC container requires nesting, mknod, and setxattr
      security.nesting: "{{ container.security_nesting | default ('false') }}"
      # note that syscalls.intercept.* requires at least Linux kernel v5.4
      # https://discuss.linuxcontainers.org/t/some-question-about-lxd/7652
      security.syscalls.intercept.mknod: "{{ container.security_syscalls_intercept_mknod | default ('false') }}"
      security.syscalls.intercept.setxattr: "{{ container.security_syscalls_intercept_setxattr | default ('false') }}"
      limits.memory: "{{ container.memory | default () }}"
      # how many CPU cores a container can use and see (default is no limit)
      limits.cpu: "{{ container.cpu | default() }}"
      # limit CPU time of a container to percentage of the total (default is no limit)
      limits.cpu.allowance: "{{ container.cpu_allowance | default() }}"
    devices:
      root:
        # required property path, even though it's set in profile
        path: /
        # required property type, even though it's set in profile
        type: "{{ container.disk_type | default ('disk') }}"
        # required property size, even though it's set in profile
        size: "{{ container.disk_space | default () }}"
        # required property pool, even though it's set in profile
        pool: "{{ container.storage_pool | default ('default') }}"
      eth0:
        # required property type, even though it's set in profile
        type: "{{ container.interface_type | default ('nic') }}"
        # required property nictype, even though it's set in profile
        nictype: "{{ container.nictype | default ('bridged') }}"
        # required property parent, even though it's set in profile
        parent: "{{ container.network | default ('lxdbr0') }}"
        ipv4.address: "{{ container.ipv4address | default () }}"
        hwaddr: "{{ container.hwaddr | default () }}"
    wait_for_ipv4_addresses: "{{ container.wait_for_ipv4_addresses | default ('false') }}"
    timeout: 600
  loop: "{{ lxd_containers }}"
  loop_control:
    loop_var: container
  register: task_lxd_container
