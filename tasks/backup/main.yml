---

# https://github.com/lxc/lxd/blob/master/doc/backup.md#instance-backups

- name: Timestamp this backup
  ansible.builtin.set_fact:
    # iso8601_basic_short should look like 20220122T223743
    this_backup_timestamp: "{{ ansible_date_time.iso8601_basic_short }}"

- ansible.builtin.set_fact:
    lxd_backup_target: "{{ backup_root_path }}/hosts/{{ inventory_hostname }}/{{ role_name }}/v{{ lxd_version }}"

- name: Make sure LXD backup root directory exists
  ansible.builtin.file:
    path: "{{ lxd_backup_target }}"
    state: directory

- name: Make sure pigz (multi-core gzip) is installed
  ansible.builtin.apt:
    name: pigz
    state: present

- ansible.builtin.debug:
    var: lxd_backup_container
  when: lxd_testing | bool

- pause:
    seconds: 10
  when: lxd_testing | bool


- name: Collect all container names to backup
  when: lxd_backup_container | length == 0
  block:

    # tried using ansible plugin community.general.lxd,
    # but could not understand its syntax.
    # anyway, we can use lxc list to get list of container names
    # supported formats: csv|json|table|yaml|compact (csv seems easiest to handle)
    - name: List all LXC containers (running or otherwise)
      ansible.builtin.command: >
        lxc list -c n -f csv
      register: lxd_containers_cmd

    - ansible.builtin.debug:
        var: lxd_containers_cmd
      when: lxd_testing | bool

    - ansible.builtin.set_fact:
        lxd_containers: "{{ lxd_containers_cmd.stdout_lines }}"

    - ansible.builtin.debug:
        var: lxd_containers[0]
      when: lxd_testing | bool
  # END OF BLOCK


- name: Set container names that will be backed up
  ansible.builtin.set_fact:
    lxd_containers: "{{ lxd_backup_container }}"
  when: lxd_backup_container | length != 0

- ansible.builtin.debug:
    var: lxd_containers
  when: lxd_testing | bool

- pause:
    seconds: 10
  when: lxd_testing | bool

- name: Stop, backup and restart each container
  ansible.builtin.include_tasks: backup/container.yml
  loop: "{{ lxd_containers }}"
  loop_control:
    loop_var: container
