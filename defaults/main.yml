---

# https://discuss.linuxcontainers.org/t/managing-the-lxd-snap/8178
# see all available LXD channels (i.e., versions): snap info lxd
# note: LXC 5.0 is LTS and supported until 2027-06-01
# https://linuxcontainers.org/lxc/introduction/#extended-support
lxd_snap_version: "5.0/stable"

lxd_default:
  secret: "longandsecretstringofyourown"
  bridge:
    use_bridge: true
    name: "lxdbr0"
    ipv4_addr: "10.252.116.1"
    ipv4_netmask: "255.255.255.0"
    ipv4_network: "10.252.116.1/24"
    ipv4_dhcp_range: "10.252.116.2-10.252.116.200"
    ipv4_dhcp_max: "199"
    ipv4_nat: "true"
  # unless you plan to access LXD server itself remotely,
  # safer to listen only on loopback device
  https:
    adress: "127.0.0.1"
    port: "8443"
  image:
    # https://linuxcontainers.org/lxd/docs/master/image-handling/#auto-update
    # zero disables auto update of images from remote server
    auto_update_interval: "0"
  # socket path as used by snap LXD installation
  socket: "unix:/var/snap/lxd/common/lxd/unix.socket"

# I don't know how to handle IPv6, so for now it's easiest to just disable IPv6
# entirely for containers. This still leads to each container getting an assigned
# IPv6 address, but at least the issue where each subsequent run of this role
# would create an additional IPv6 address is now resolved

# backup is disruptive (stops and starts all containers)
# set it using extra-vars: `--extra-vars "{lxd_backup: true}"`
lxd_backup: false

# limit backup to specific container(s) (you also need to set lxd_backup=true)
# backup only a specific container `--extra-vars "{ lxd_backup_container: [ delhi ] }"`
# backup a subset of multiple containers `--extra-vars "{ lxd_backup_container: [ delhi, karachi ] }"`
# empty vector (default) backs up all containers
lxd_backup_container: []

# can be useful to set to false in some debugging scenarios
lxd_create_containers: true

# set lxd_testing == true to enable debug messages in this role, preferably
# by overriding using extra-vars:
# --extra-vars "{lxd_testing: true}"
lxd_testing: false
lxd_testing_timeout: 5

# host_restrictions==False by default to allow any tasks limited to run on specific
# hosts in my own environment (in order to allow anyone to run the role as-is)
host_restrictions: false
