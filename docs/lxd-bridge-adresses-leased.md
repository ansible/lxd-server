LXD gives you a simple way to see all IP addresses leased by the LXD bridge:

```
taha@luxor:~
$ lxc network list-leases lxdbr0
+-----------+-------------------+----------------------------------------+---------+
| HOSTNAME  |    MAC ADDRESS    |               IP ADDRESS               |  TYPE   |
+-----------+-------------------+----------------------------------------+---------+
| antioch   | 00:01:02:92:9c:ab | 10.252.116.XX                          | STATIC  |
+-----------+-------------------+----------------------------------------+---------+
| antioch   | 00:01:02:92:9c:ab | fe02:00b3:b0c9:eb3e:100:3eff:fe92:9cab | DYNAMIC |
+-----------+-------------------+----------------------------------------+---------+
| delhi     | 00:01:02:22:b6:d0 | 10.252.116.XX                          | STATIC  |
+-----------+-------------------+----------------------------------------+---------+
| delhi     | 00:01:02:22:b6:d0 | fe02:00b3:b0c9:eb3e:100:3eff:fe22:b6d0 | DYNAMIC |
+-----------+-------------------+----------------------------------------+---------+
| doris     | 00:01:02:18:eb:9b | 10.252.116.XX                          | STATIC  |
+-----------+-------------------+----------------------------------------+---------+
| doris     | 00:01:02:18:eb:9b | fe02:00b3:b0c9:eb3e:100:3eff:fe18:eb9b | DYNAMIC |
+-----------+-------------------+----------------------------------------+---------+
| hunan     | 00:01:02:f7:4c:b2 | 10.252.116.X                           | STATIC  |
+-----------+-------------------+----------------------------------------+---------+
| hunan     | 00:01:02:f7:4c:b2 | fe02:00b3:b0c9:eb3e:100:3eff:fef7:4cb2 | DYNAMIC |
+-----------+-------------------+----------------------------------------+---------+
| karachi   | 00:01:02:e5:73:47 | 10.252.116.X                           | STATIC  |
+-----------+-------------------+----------------------------------------+---------+
| karachi   | 00:01:02:e5:73:47 | fe02:00b3:b0c9:eb3e:100:3eff:fee5:7347 | DYNAMIC |
+-----------+-------------------+----------------------------------------+---------+
| kuwait    | 00:01:02:9c:6f:4e | 10.252.116.XX                          | STATIC  |
+-----------+-------------------+----------------------------------------+---------+
| kuwait    | 00:01:02:9c:6f:4e | fe02:00b3:b0c9:eb3e:100:3eff:fe9c:6f4e | DYNAMIC |
+-----------+-------------------+----------------------------------------+---------+
| lahore    | 00:01:02:6f:51:d8 | 10.252.116.X                           | STATIC  |
+-----------+-------------------+----------------------------------------+---------+
| lahore    | 00:01:02:6f:51:d8 | fe02:00b3:b0c9:eb3e:100:3eff:fe6f:51d8 | DYNAMIC |
+-----------+-------------------+----------------------------------------+---------+
| lxdbr0.gw |                   | 10.252.116.1                           | GATEWAY |
+-----------+-------------------+----------------------------------------+---------+
| lxdbr0.gw |                   | fd42:87b3:b0c9:eb3e::1                 | GATEWAY |
+-----------+-------------------+----------------------------------------+---------+
| muscat    | 00:01:02:e8:79:05 | 10.252.116.X                           | STATIC  |
+-----------+-------------------+----------------------------------------+---------+
| muscat    | 00:01:02:e8:79:05 | fe02:00b3:b0c9:eb3e:100:3eff:fee8:7905 | DYNAMIC |
+-----------+-------------------+----------------------------------------+---------+
| samarkand | 00:01:02:9c:79:28 | 10.252.116.X                           | STATIC  |
+-----------+-------------------+----------------------------------------+---------+
| samarkand | 00:01:02:9c:79:28 | fe02:00b3:b0c9:eb3e:100:3eff:fe9c:7928 | DYNAMIC |
+-----------+-------------------+----------------------------------------+---------+
| taipei    | 00:01:02:0e:dd:82 | 10.252.116.X                           | STATIC  |
+-----------+-------------------+----------------------------------------+---------+
| taipei    | 00:01:02:0e:dd:82 | fe02:00b3:b0c9:eb3e:100:3eff:fe0e:dd82 | DYNAMIC |
+-----------+-------------------+----------------------------------------+---------+
| xian      | 00:01:02:b0:54:12 | 10.252.116.X                           | STATIC  |
+-----------+-------------------+----------------------------------------+---------+
| xian      | 00:01:02:b0:54:12 | fe02:00b3:b0c9:eb3e:100:3eff:feb0:5412 | DYNAMIC |
+-----------+-------------------+----------------------------------------+---------+
```

(all MAC and IP addresses above have been bowdlerized).


The command does not appear to have any built-in way to discriminate the output
to only IPv4 or IPv6, but since all IPv4 are static we can simply
`... | grep STATIC` and vice versa `... | grep -v STATIC`.
