# Upgrade LXD v3 (apt) to v4 (snap) on Bionic

LXD v3 does not support the `backup` functionality introduced with later versions.
I think we might need to try to upgrade LXD in place (without destroying our containers)
in order to perform proper backups before migrating LXD to the new host.

But to be able to attempt the upgrade, we first need some sort of backup...


## Backup of LXD v3.0.3

> Ubuntu 18.04 came with LXD 3.0.3 installed as an apt package,
> whereas LXD 4.4 only comes as a snap package.
> To remove the LXD apt package, run `lxd.migrate` after installing the snap package,
> which will run through the process of migrating containers and config over to
> the new version and remove the old one.
> https://discuss.linuxcontainers.org/t/lxd-version-with-snap-get-4-4-and-show-3-0-3/8853/2

I can confirm all services on all containers appear to be working normally
after reflashing the SSD from the dd backup.

Let's stop all containers and backup the entire LXD tree.

```
taha@luxor:/
$ lxc info
config:
  core.https_address: 127.0.0.1:8443
  core.trust_password: true
  images.auto_update_interval: "0"
api_extensions:
- storage_zfs_remove_snapshots
- container_host_shutdown_timeout
- container_stop_priority
- container_syscall_filtering
- auth_pki
- container_last_used_at
- etag
- patch
- usb_devices
- https_allowed_credentials
- image_compression_algorithm
- directory_manipulation
- container_cpu_time
- storage_zfs_use_refquota
- storage_lvm_mount_options
- network
- profile_usedby
- container_push
- container_exec_recording
- certificate_update
- container_exec_signal_handling
- gpu_devices
- container_image_properties
- migration_progress
- id_map
- network_firewall_filtering
- network_routes
- storage
- file_delete
- file_append
- network_dhcp_expiry
- storage_lvm_vg_rename
- storage_lvm_thinpool_rename
- network_vlan
- image_create_aliases
- container_stateless_copy
- container_only_migration
- storage_zfs_clone_copy
- unix_device_rename
- storage_lvm_use_thinpool
- storage_rsync_bwlimit
- network_vxlan_interface
- storage_btrfs_mount_options
- entity_description
- image_force_refresh
- storage_lvm_lv_resizing
- id_map_base
- file_symlinks
- container_push_target
- network_vlan_physical
- storage_images_delete
- container_edit_metadata
- container_snapshot_stateful_migration
- storage_driver_ceph
- storage_ceph_user_name
- resource_limits
- storage_volatile_initial_source
- storage_ceph_force_osd_reuse
- storage_block_filesystem_btrfs
- resources
- kernel_limits
- storage_api_volume_rename
- macaroon_authentication
- network_sriov
- console
- restrict_devlxd
- migration_pre_copy
- infiniband
- maas_network
- devlxd_events
- proxy
- network_dhcp_gateway
- file_get_symlink
- network_leases
- unix_device_hotplug
- storage_api_local_volume_handling
- operation_description
- clustering
- event_lifecycle
- storage_api_remote_volume_handling
- nvidia_runtime
- candid_authentication
- candid_config
- candid_config_key
- usb_optional_vendorid
api_status: stable
api_version: "1.0"
auth: trusted
public: false
auth_methods:
- tls
environment:
  addresses:
  - 127.0.0.1:8443
  architectures:
  - x86_64
  - i686
  certificate: |
    -----BEGIN CERTIFICATE-----
    MIIFWzCCA0OgAwIBAgIQfk+ez9GtnbfxxVs4JmMLADANBgkqhkiG9w0BAQsFADAz
    [...]
    nVAOHi/NgRCSqmw4LDo6/blEkh3MMVbzXak9FhqUDXF71eH6BhPEwghkvxFyvP+P
    4Rf6syZIXftDdxEaYXIWlRixv2wi96bIjFVu6uuw1w==
    -----END CERTIFICATE-----
  certificate_fingerprint: 3cf9927548f864738dfa19a08ca622f1a1411d7b675421d3914ace77aef61f1b
  driver: lxc
  driver_version: 3.0.3
  kernel: Linux
  kernel_architecture: x86_64
  kernel_version: 4.15.0-176-generic
  server: lxd
  server_pid: 2273
  server_version: 3.0.3
  storage: dir
  storage_version: "1"
  server_clustered: false
  server_name: luxor
  project: ""
```

```
$ lxc list
+---------+---------+----------------------------+------------------------------------------------------+------------+-----------+
|  NAME   |  STATE  |            IPV4            |                         IPV6                         |    TYPE    | SNAPSHOTS |
+---------+---------+----------------------------+------------------------------------------------------+------------+-----------+
| antioch | RUNNING | 10.252.116.11 (eth0)       | fd42:31a9:dbce:b581:216:3eff:fe92:9cab (eth0)        | PERSISTENT | 1         |
+---------+---------+----------------------------+------------------------------------------------------+------------+-----------+
| beijing | RUNNING | 10.252.116.6 (eth0)        | fd42:31a9:dbce:b581:216:3eff:feb6:c12 (eth0)         | PERSISTENT | 1         |
+---------+---------+----------------------------+------------------------------------------------------+------------+-----------+
| bilbeis | RUNNING | 192.168.1.103 (eth0)       | fd7a:115c:a1e0:ab12:4843:cd96:6262:cb14 (tailscale0) | PERSISTENT | 1         |
|         |         | 100.98.203.20 (tailscale0) |                                                      |            |           |
+---------+---------+----------------------------+------------------------------------------------------+------------+-----------+
| canton  | RUNNING | 192.168.1.105 (eth0)       |                                                      | PERSISTENT | 1         |
+---------+---------+----------------------------+------------------------------------------------------+------------+-----------+
| delhi   | RUNNING | 10.252.116.10 (eth0)       | fd42:31a9:dbce:b581:216:3eff:fe33:22b5 (eth0)        | PERSISTENT | 1         |
+---------+---------+----------------------------+------------------------------------------------------+------------+-----------+
| hunan   | RUNNING | 10.252.116.4 (eth0)        | fd42:31a9:dbce:b581:216:3eff:fe57:b53a (eth0)        | PERSISTENT | 1         |
+---------+---------+----------------------------+------------------------------------------------------+------------+-----------+
| karachi | RUNNING | 10.252.116.7 (eth0)        | fd42:31a9:dbce:b581:216:3eff:fee5:7347 (eth0)        | PERSISTENT | 1         |
+---------+---------+----------------------------+------------------------------------------------------+------------+-----------+
| lahore  | RUNNING | 10.252.116.8 (eth0)        | fd42:31a9:dbce:b581:216:3eff:fe6f:51d8 (eth0)        | PERSISTENT | 1         |
+---------+---------+----------------------------+------------------------------------------------------+------------+-----------+
| muscat  | RUNNING | 10.252.116.9 (eth0)        | fd42:31a9:dbce:b581:216:3eff:fec9:7842 (eth0)        | PERSISTENT | 1         |
+---------+---------+----------------------------+------------------------------------------------------+------------+-----------+
| taipei  | RUNNING | 10.252.116.3 (eth0)        | fd42:31a9:dbce:b581:216:3eff:fe0e:dd82 (eth0)        | PERSISTENT | 1         |
+---------+---------+----------------------------+------------------------------------------------------+------------+-----------+
| xian    | RUNNING | 10.252.116.5 (eth0)        | fd42:31a9:dbce:b581:216:3eff:fe3b:f352 (eth0)        | PERSISTENT | 1         |
+---------+---------+----------------------------+------------------------------------------------------+------------+-----------+
```

```
$ dpkg -l | grep lxd
ii  lxd                                       3.0.3-0ubuntu1~18.04.1                          amd64        Container hypervisor based on LXC - daemon
ii  lxd-client                                3.0.3-0ubuntu1~18.04.1                          amd64        Container hypervisor based on LXC - client
ii  lxd-tools                                 3.0.3-0ubuntu1~18.04.1                          amd64        Container hypervisor based on LXC - extra tools

$ snap list
Name     Version    Rev    Tracking       Publisher     Notes
certbot  1.29.0     2192   latest/stable  certbot-eff✓  classic
core     16-2.56.2  13425  latest/stable  canonical✓    core
core20   20220719   1587   latest/stable  canonical✓    base
```


To save some time during this massive rsync, let's remove all existing snapshots
(won't need them anymore).

+ https://discuss.linuxcontainers.org/t/how-to-delete-multimple-snapshots-with-one-command/6889/2

```
$ lxc delete xian/before-luxor-upgrade
# and repeat for all the containers...
```

How large is the LXD tree?
```
root@luxor:~
# du -hx --max-depth 1 /var/lib/lxd | sort -hr
149G	/var/lib/lxd
93G	/var/lib/lxd/storage-pools
56G	/var/lib/lxd/devices
800M	/var/lib/lxd/images
41M	/var/lib/lxd/database
572K	/var/lib/lxd/security
60K	/var/lib/lxd/networks
4.0K	/var/lib/lxd/snapshots
4.0K	/var/lib/lxd/disks
4.0K	/var/lib/lxd/containers
```

In case disaster strikes, this will be our backup:

```
# lxc stop antioch beijing bilbeis canton delhi hunan karachi lahore muscat taipei xian
# rsync -rlptgoDEAXv /var/lib/lxd /media/bay/backup/hosts/luxor/lxd-v3.0.3
# rsync -a /etc/subuid /media/bay/backup/hosts/luxor/lxd-v3.0.3/
# rsync -a /etc/subgid /media/bay/backup/hosts/luxor/lxd-v3.0.3/
```

Install LXD v4 snap (this should not affect the LXD apt install)
by running this role with the following variables set:
```
lxd_snap_version: "4.0/stable"
lxd_create_profiles: false
lxd_create_containers: false
```

NOTE: at this point, we have both apt and snap LXD on the system, and attempting
to run the snap LXD fails with an informative message:
```
root@luxor:/media/bay/backup/hosts/luxor
# type -a lxc
lxc is /usr/bin/lxc
lxc is /snap/bin/lxc
root@luxor:/media/bay/backup/hosts/luxor
# /snap/bin/lxc info
Error: Both native and snap packages are installed on this system
       Run "lxd.migrate" to complete your migration to the snap package
```

Next, migrate everything (containers, profiles, networks, etc.) from apt to snap LXD

+ https://discuss.linuxcontainers.org/t/how-to-move-containers-from-apt-lxd-to-snap-lxd-import-export/4035
+ https://discuss.linuxcontainers.org/t/lxd-version-with-snap-get-4-4-and-show-3-0-3/8853
+ https://blog.simos.info/how-to-migrate-lxd-from-deb-ppa-package-to-snap-package/

```
root@luxor:/
# lxd.migrate
=> Connecting to source server
=> Connecting to destination server
=> Running sanity checks

=== Source server
LXD version: 3.0.3
LXD PID: 2273
Resources:
  Containers: 11
  Images: 2
  Networks: 1
  Storage pools: 1

=== Destination server
LXD version: 4.0.9
LXD PID: 22899
Resources:
  Containers: 0
  Images: 0
  Networks: 0
  Storage pools: 0

The migration process will shut down all your containers then move your data to the destination LXD.
Once the data is moved, the destination LXD will start and apply any needed updates.
And finally your containers will be brought back to their previous state, completing the migration.

Are you ready to proceed (yes/no) [default=no]? yes
=> Shutting down the source LXD
=> Stopping the source LXD units
=> Stopping the destination LXD unit
=> Unmounting source LXD paths
=> Unmounting destination LXD paths
=> Wiping destination LXD clean
=> Backing up the database
=> Moving the data
=> Updating the storage backends
=> Starting the destination LXD
=> Waiting for LXD to come online

=== Destination server
LXD version: 4.0.9
LXD PID: 23352
Resources:
  Containers: 11
  Images: 2
  Networks: 1
  Storage pools: 1

The migration is now complete and your containers should be back online.
Do you want to uninstall the old LXD (yes/no) [default=yes]? yes

All done. You may need to close your current shell and open a new one to have the "lxc" command work.
To migrate your existing client configuration, move ~/.config/lxc to ~/snap/lxd/common/config
```

To complete cleanup of apt LXD, we should also uninstall `lxd-tools`. OK.

`lxc list` shows all containers up and running:
```
taha@luxor:~
$ lxc list --fast
+---------+---------+--------------+----------------------+------------------+-----------+
|  NAME   |  STATE  | ARCHITECTURE |      CREATED AT      |     PROFILES     |   TYPE    |
+---------+---------+--------------+----------------------+------------------+-----------+
| antioch | RUNNING | x86_64       | 2022/07/22 04:14 UTC | calibreweb       | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| beijing | RUNNING | x86_64       | 2021/12/11 06:18 UTC | default          | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| bilbeis | RUNNING | x86_64       | 2021/06/27 02:12 UTC | dns-server       | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| canton  | RUNNING | x86_64       | 2022/02/23 20:31 UTC | piratebox        | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| delhi   | RUNNING | x86_64       | 2022/07/24 04:07 UTC | jellypirate      | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| hunan   | RUNNING | x86_64       | 2021/06/26 22:54 UTC | rstudio-server   | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| karachi | RUNNING | x86_64       | 2022/01/13 06:14 UTC | nextcloud-server | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| lahore  | RUNNING | x86_64       | 2022/01/09 19:56 UTC | nextcloud-server | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| muscat  | RUNNING | x86_64       | 2022/04/07 01:34 UTC | default          | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| taipei  | RUNNING | x86_64       | 2022/02/13 00:38 UTC | archivebox       | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| xian    | RUNNING | x86_64       | 2021/06/26 22:54 UTC | rstudio-server   | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
```

```
$ lxc info
config:
  core.https_address: 127.0.0.1:8443
  core.trust_password: true
  images.auto_update_interval: "0"
api_extensions:
- storage_zfs_remove_snapshots
- container_host_shutdown_timeout
- container_stop_priority
- container_syscall_filtering
- auth_pki
- container_last_used_at
- etag
- patch
- usb_devices
- https_allowed_credentials
- image_compression_algorithm
- directory_manipulation
- container_cpu_time
- storage_zfs_use_refquota
- storage_lvm_mount_options
- network
- profile_usedby
- container_push
- container_exec_recording
- certificate_update
- container_exec_signal_handling
- gpu_devices
- container_image_properties
- migration_progress
- id_map
- network_firewall_filtering
- network_routes
- storage
- file_delete
- file_append
- network_dhcp_expiry
- storage_lvm_vg_rename
- storage_lvm_thinpool_rename
- network_vlan
- image_create_aliases
- container_stateless_copy
- container_only_migration
- storage_zfs_clone_copy
- unix_device_rename
- storage_lvm_use_thinpool
- storage_rsync_bwlimit
- network_vxlan_interface
- storage_btrfs_mount_options
- entity_description
- image_force_refresh
- storage_lvm_lv_resizing
- id_map_base
- file_symlinks
- container_push_target
- network_vlan_physical
- storage_images_delete
- container_edit_metadata
- container_snapshot_stateful_migration
- storage_driver_ceph
- storage_ceph_user_name
- resource_limits
- storage_volatile_initial_source
- storage_ceph_force_osd_reuse
- storage_block_filesystem_btrfs
- resources
- kernel_limits
- storage_api_volume_rename
- macaroon_authentication
- network_sriov
- console
- restrict_devlxd
- migration_pre_copy
- infiniband
- maas_network
- devlxd_events
- proxy
- network_dhcp_gateway
- file_get_symlink
- network_leases
- unix_device_hotplug
- storage_api_local_volume_handling
- operation_description
- clustering
- event_lifecycle
- storage_api_remote_volume_handling
- nvidia_runtime
- container_mount_propagation
- container_backup
- devlxd_images
- container_local_cross_pool_handling
- proxy_unix
- proxy_udp
- clustering_join
- proxy_tcp_udp_multi_port_handling
- network_state
- proxy_unix_dac_properties
- container_protection_delete
- unix_priv_drop
- pprof_http
- proxy_haproxy_protocol
- network_hwaddr
- proxy_nat
- network_nat_order
- container_full
- candid_authentication
- backup_compression
- candid_config
- nvidia_runtime_config
- storage_api_volume_snapshots
- storage_unmapped
- projects
- candid_config_key
- network_vxlan_ttl
- container_incremental_copy
- usb_optional_vendorid
- snapshot_scheduling
- snapshot_schedule_aliases
- container_copy_project
- clustering_server_address
- clustering_image_replication
- container_protection_shift
- snapshot_expiry
- container_backup_override_pool
- snapshot_expiry_creation
- network_leases_location
- resources_cpu_socket
- resources_gpu
- resources_numa
- kernel_features
- id_map_current
- event_location
- storage_api_remote_volume_snapshots
- network_nat_address
- container_nic_routes
- rbac
- cluster_internal_copy
- seccomp_notify
- lxc_features
- container_nic_ipvlan
- network_vlan_sriov
- storage_cephfs
- container_nic_ipfilter
- resources_v2
- container_exec_user_group_cwd
- container_syscall_intercept
- container_disk_shift
- storage_shifted
- resources_infiniband
- daemon_storage
- instances
- image_types
- resources_disk_sata
- clustering_roles
- images_expiry
- resources_network_firmware
- backup_compression_algorithm
- ceph_data_pool_name
- container_syscall_intercept_mount
- compression_squashfs
- container_raw_mount
- container_nic_routed
- container_syscall_intercept_mount_fuse
- container_disk_ceph
- virtual-machines
- image_profiles
- clustering_architecture
- resources_disk_id
- storage_lvm_stripes
- vm_boot_priority
- unix_hotplug_devices
- api_filtering
- instance_nic_network
- clustering_sizing
- firewall_driver
- projects_limits
- container_syscall_intercept_hugetlbfs
- limits_hugepages
- container_nic_routed_gateway
- projects_restrictions
- custom_volume_snapshot_expiry
- volume_snapshot_scheduling
- trust_ca_certificates
- snapshot_disk_usage
- clustering_edit_roles
- container_nic_routed_host_address
- container_nic_ipvlan_gateway
- resources_usb_pci
- resources_cpu_threads_numa
- resources_cpu_core_die
- api_os
- resources_system
- usedby_consistency
- resources_gpu_mdev
- console_vga_type
- projects_limits_disk
- storage_rsync_compression
- gpu_mdev
- resources_pci_iommu
- resources_network_usb
- resources_disk_address
- network_state_vlan
- gpu_sriov
- migration_stateful
- disk_state_quota
- storage_ceph_features
- gpu_mig
- clustering_join_token
- clustering_description
- server_trusted_proxy
- clustering_update_cert
- storage_api_project
- server_instance_driver_operational
- server_supported_storage_drivers
- event_lifecycle_requestor_address
- resources_gpu_usb
- network_counters_errors_dropped
- image_source_project
- database_leader
- instance_all_projects
- ceph_rbd_du
- qemu_metrics
- gpu_mig_uuid
- event_project
- instance_allow_inconsistent_copy
- image_restrictions
api_status: stable
api_version: "1.0"
auth: trusted
public: false
auth_methods:
- tls
environment:
  addresses:
  - 127.0.0.1:8443
  architectures:
  - x86_64
  - i686
  certificate: |
    -----BEGIN CERTIFICATE-----
    MIIFWzCCA0OgAwIBAgIQfk+ez9GtnbfxxVs4JmMLADANBgkqhkiG9w0BAQsFADAz
    MRwwGgYDVQQKExNsaW51eGNvbnRhaW5lcnMub3JnMRMwEQYDVQQDDApyb290QGx1
    [...]
    nVAOHi/NgRCSqmw4LDo6/blEkh3MMVbzXak9FhqUDXF71eH6BhPEwghkvxFyvP+P
    4Rf6syZIXftDdxEaYXIWlRixv2wi96bIjFVu6uuw1w==
    -----END CERTIFICATE-----
  certificate_fingerprint: 3cf9927548f864738dfa19a08ca622f1a1411d7b675421d3914ace77aef61f1b
  driver: lxc | qemu
  driver_version: 4.0.12 | 6.1.1
  firewall: xtables
  kernel: Linux
  kernel_architecture: x86_64
  kernel_features:
    netnsid_getifaddrs: "false"
    seccomp_listener: "false"
    seccomp_listener_continue: "false"
    shiftfs: "false"
    uevent_injection: "false"
    unpriv_fscaps: "true"
  kernel_version: 4.15.0-176-generic
  lxc_features:
    cgroup2: "true"
    core_scheduling: "true"
    devpts_fd: "true"
    idmapped_mounts_v2: "true"
    mount_injection_file: "true"
    network_gateway_device_route: "true"
    network_ipvlan: "true"
    network_l2proxy: "true"
    network_phys_macvlan_mtu: "true"
    network_veth_router: "true"
    pidfd: "true"
    seccomp_allow_deny_syntax: "true"
    seccomp_notify: "true"
    seccomp_proxy_send_notify_fd: "true"
  os_name: Ubuntu
  os_version: "18.04"
  project: default
  server: lxd
  server_clustered: false
  server_name: luxor
  server_pid: 23352
  server_version: 4.0.9
  storage: dir
  storage_version: "1"
  storage_supported_drivers:
  - name: zfs
    version: 0.7.5-1ubuntu16.12
    remote: false
  - name: ceph
    version: 15.2.14
    remote: true
  - name: btrfs
    version: 5.4.1
    remote: false
  - name: cephfs
    version: 15.2.14
    remote: true
  - name: dir
    version: "1"
    remote: false
  - name: lvm
    version: 2.03.07(2) (2019-11-30) / 1.02.167 (2019-11-30) / 4.37.0
    remote: false
```

LXD v4.0.9 (snap) working OK. All containers OK, except for `netbox` on `taipei`
which used a "trick" to access its static web-directory inside the container
from the LXD host. This is no longer working, I assume because of how snap
mounts LXD in some sort of isolated manner. Will have to look into whether to
change the LXD behaviour, or simply run a web server inside that container.

Other than that, all containers OK on LXD v4.0.9.

Now let's utilise the improved backup tools in LXD v4 to create a backup
of each container.


## Instance backups

+ https://github.com/lxc/lxd/blob/master/doc/backup.md#instance-backups

> These tarballs can be saved any way you want on any filesystem you want and
> can be imported back into LXD using the `lxc import` command.

Stop all containers and export all instances (by using `pigz` we significantly speed up the compression):
```
root@luxor:~
# mkdir /media/bay/backup/hosts/luxor/lxd-v4.0.9/instance-exports
# lxc stop antioch beijing bilbeis canton delhi hunan karachi lahore muscat taipei xian
lxc export antioch /media/bay/backup/hosts/luxor/lxd-v4.0.9/instance-exports/antioch.tar.gz --compression pigz
lxc export beijing /media/bay/backup/hosts/luxor/lxd-v4.0.9/instance-exports/beijing.tar.gz --compression pigz
lxc export bilbeis /media/bay/backup/hosts/luxor/lxd-v4.0.9/instance-exports/bilbeis.tar.gz --compression pigz
lxc export canton /media/bay/backup/hosts/luxor/lxd-v4.0.9/instance-exports/canton.tar.gz --compression pigz
lxc export delhi /media/bay/backup/hosts/luxor/lxd-v4.0.9/instance-exports/delhi.tar.gz --compression pigz
lxc export hunan /media/bay/backup/hosts/luxor/lxd-v4.0.9/instance-exports/hunan.tar.gz --compression pigz
lxc export karachi /media/bay/backup/hosts/luxor/lxd-v4.0.9/instance-exports/karachi.tar.gz --compression pigz
lxc export lahore /media/bay/backup/hosts/luxor/lxd-v4.0.9/instance-exports/lahore.tar.gz --compression pigz
lxc export muscat /media/bay/backup/hosts/luxor/lxd-v4.0.9/instance-exports/muscat.tar.gz --compression pigz
lxc export taipei /media/bay/backup/hosts/luxor/lxd-v4.0.9/instance-exports/taipei.tar.gz --compression pigz
lxc export xian /media/bay/backup/hosts/luxor/lxd-v4.0.9/instance-exports/xian.tar.gz --compression pigz
```

Let's upgrade to latest LTS version, which is also what's installed by default
on Jammy.


## Upgrade from LXD v4 to v5 (latest LTS)

+ https://discuss.linuxcontainers.org/t/managing-the-lxd-snap/8178
+ https://linuxcontainers.org/lxd/news/#lxd-50-lts-has-been-released

```
sudo snap install lxd --channel=5.0/stable
```
