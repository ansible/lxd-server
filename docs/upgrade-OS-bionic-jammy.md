# Migrating containers from bionic to jammy

We are going from LXD version 3.0.3 to 5.0.0 (default on Jammy).

First attempt.
Essentially transplanted snapshots into the new LXD tree, also replaced the
`database` directory with backup.

```
root@luxor:/var/snap/lxd/common/lxd
# systemctl restart snap.lxd.daemon.service
# systemctl status snap.lxd.daemon.service
● snap.lxd.daemon.service - Service for snap application lxd.daemon
     Loaded: loaded (/etc/systemd/system/snap.lxd.daemon.service; static)
     Active: active (running) since Wed 2022-07-27 14:23:41 CEST; 646ms ago
TriggeredBy: ● snap.lxd.daemon.unix.socket
   Main PID: 1014872 (daemon.start)
      Tasks: 0 (limit: 38018)
     Memory: 420.0K
        CPU: 110ms
     CGroup: /system.slice/snap.lxd.daemon.service
             ‣ 1014872 /bin/sh /snap/lxd/22923/commands/daemon.start

jul 27 14:23:41 luxor lxd.daemon[1014283]: - shared_pidns
jul 27 14:23:41 luxor lxd.daemon[1014283]: - cpuview_daemon
jul 27 14:23:41 luxor lxd.daemon[1014283]: - loadavg_daemon
jul 27 14:23:41 luxor lxd.daemon[1014283]: - pidfds
jul 27 14:23:41 luxor lxd.daemon[1014283]: Reloaded LXCFS
jul 27 14:23:41 luxor lxd.daemon[1014872]: => Re-using existing LXCFS
jul 27 14:23:41 luxor lxd.daemon[1014872]: => Starting LXD
jul 27 14:23:41 luxor lxd.daemon[1015019]: time="2022-07-27T14:23:41+02:00" level=warning msg=" - Couldn't find the CGroup network priority controller, network priority will be ignored"
jul 27 14:23:41 luxor lxd.daemon[1015019]: time="2022-07-27T14:23:41+02:00" level=error msg="Failed to start the daemon" err="Unsupported upgrade path, please first upgrade to LXD 4.0"
jul 27 14:23:41 luxor lxd.daemon[1015019]: Error: Unsupported upgrade path, please first upgrade to LXD 4.0
```

It seems we need to learn how to install a specific snap package version
(effectively we need to downgrade LXD).



## Downgrading LXD to v4 using snap

See `tasks/revert-version.yml`.

Interesting errors after what appears to be a successful downgrade to v4:

```
root@luxor:/
# lxc info
Error: Get "http://unix.socket/1.0": dial unix /var/snap/lxd/common/lxd/unix.socket: connect: connection refused
# /snap/bin/lxc query --wait -X GET /1.0
Error: Get "http://unix.socket/1.0": dial unix /var/snap/lxd/common/lxd/unix.socket: connect: connection refused
# sudo -u lxd /snap/bin/lxc query --wait -X GET /1.0
2022/07/27 16:07:49.397368 cmd_run.go:1044: WARNING: cannot create user data directory: cannot create snap home dir: mkdir /var/snap/lxd/common/lxd/snap: permission denied
cannot create user data directory: /var/snap/lxd/common/lxd/snap/lxd/22753: Permission denied
```

```
# lxd --debug --group lxd
DBUG[07-27|16:12:03] Connecting to a local LXD over a Unix socket
DBUG[07-27|16:12:03] Sending request to LXD                   method=GET url=http://unix.socket/1.0 etag=
INFO[07-27|16:12:03] LXD is starting                          version=4.0.9 mode=normal path=/var/snap/lxd/common/lxd
INFO[07-27|16:12:03] Kernel uid/gid map:
INFO[07-27|16:12:03]  - u 0 0 4294967295
INFO[07-27|16:12:03]  - g 0 0 4294967295
INFO[07-27|16:12:03] Configured LXD uid/gid map:
INFO[07-27|16:12:03]  - u 0 1000000 1000000000
INFO[07-27|16:12:03]  - g 0 1000000 1000000000
INFO[07-27|16:12:03] Kernel features:
INFO[07-27|16:12:03]  - closing multiple file descriptors efficiently: yes
INFO[07-27|16:12:03]  - netnsid-based network retrieval: yes
INFO[07-27|16:12:03]  - pidfds: yes
INFO[07-27|16:12:03]  - core scheduling: yes
INFO[07-27|16:12:03]  - uevent injection: yes
INFO[07-27|16:12:03]  - seccomp listener: yes
INFO[07-27|16:12:03]  - seccomp listener continue syscalls: yes
INFO[07-27|16:12:03]  - seccomp listener add file descriptors: yes
INFO[07-27|16:12:03]  - attach to namespaces via pidfds: yes
INFO[07-27|16:12:03]  - safe native terminal allocation : yes
INFO[07-27|16:12:03]  - unprivileged file capabilities: yes
INFO[07-27|16:12:03]  - cgroup layout: cgroup2
WARN[07-27|16:12:03]  - Couldn't find the CGroup hugetlb controller, hugepage limits will be ignored
WARN[07-27|16:12:03]  - Couldn't find the CGroup network priority controller, network priority will be ignored
INFO[07-27|16:12:03]  - shiftfs support: yes
INFO[07-27|16:12:03] Initializing local database
DBUG[07-27|16:12:03] Refreshing local trusted certificate cache
INFO[07-27|16:12:03] Set client certificate to server certificate fingerprint=5606178cb9b331b6ce4a21a87570bdf9ed7b13b0990852593534ffda62ba1ead
DBUG[07-27|16:12:03] Initializing database gateway
INFO[07-27|16:12:03] Starting database node                   id=1 local=1 role=voter
DBUG[07-27|16:12:03] Connecting to a local LXD over a Unix socket
DBUG[07-27|16:12:03] Sending request to LXD                   method=GET url=http://unix.socket/1.0 etag=
DBUG[07-27|16:12:03] Detected stale unix socket, deleting
INFO[07-27|16:12:03] Starting /dev/lxd handler:
INFO[07-27|16:12:03]  - binding devlxd socket                 socket=/var/snap/lxd/common/lxd/devlxd/sock
INFO[07-27|16:12:03] REST API daemon:
INFO[07-27|16:12:03]  - binding Unix socket                   socket=/var/snap/lxd/common/lxd/unix.socket
INFO[07-27|16:12:03]  - binding TCP socket                    socket=127.0.0.1:8443
INFO[07-27|16:12:03] Initializing global database
INFO[07-27|16:12:03] Connecting to global database
DBUG[07-27|16:12:03] Dqlite: attempt 1: server 1: connected
INFO[07-27|16:12:03] Connected to global database
DBUG[07-27|16:12:03] Database error: failed to fetch current nodes versions: no such column: pending
EROR[07-27|16:12:03] Failed to start the daemon               err="Failed to initialize global database: failed to ensure schema: failed to fetch current nodes versions: no such column: pending"
INFO[07-27|16:12:03] Starting shutdown sequence               signal=interrupt
DBUG[07-27|16:12:03] Cancel ongoing or future gRPC connection attempts
INFO[07-27|16:12:03] Stop database gateway
INFO[07-27|16:12:04] Stopping REST API handler:
INFO[07-27|16:12:04]  - closing socket                        socket=127.0.0.1:8443
INFO[07-27|16:12:04]  - closing socket                        socket=/var/snap/lxd/common/lxd/unix.socket
INFO[07-27|16:12:04] Stopping /dev/lxd handler:
INFO[07-27|16:12:04]  - closing socket                        socket=/var/snap/lxd/common/lxd/devlxd/sock
INFO[07-27|16:12:04] Not unmounting temporary filesystems (instances are still running)
INFO[07-27|16:12:04] Daemon stopped
Error: Failed to initialize global database: failed to ensure schema: failed to fetch current nodes versions: no such column: pending
```

So is it the database from v3 that's causes this?
How do I reinstall lxd from scratch?

+ https://discuss.linuxcontainers.org/t/remove-or-restore-lxd/6897/2
+ https://discuss.linuxcontainers.org/t/how-to-remove-lxd-from-my-system/2336/4
+ https://forum.snapcraft.io/t/automatic-snapshots-snap-remove-purge-proposal/11294/2
+ https://discuss.linuxcontainers.org/t/sudo-snap-remove-lxd-seems-to-take-a-very-long-time/8712

Very important to use `--purge`, otherwise command takes forever (literally hours)
and ends up writing a huge zip-file in `/var/lib/snapd/snapshots/`.

```
# snap remove --purge lxd
```

This seems to have indeed completely nuked lxd from the system.

Reran this role (with `lxd_snap_version: "4.0/stable"`) and now we have
a working LXD v4.0.9 instance.


## Try migrating our snapshots again

+ https://github.com/lxc/lxd/blob/master/doc/backup.md

```
# systemctl stop snap.lxd.daemon.service
# systemctl stop snap.lxd.daemon.unix.socket
```

```
root@luxor:/var/snap/lxd/common/lxd
# rm -rf database/
# cp -ar /media/bay/backup/hosts/luxor/lxd/database .
# cp -ar /media/bay/backup/hosts/luxor/lxd/devlxd/* devlxd/
# rm -rf networks/
# cp -ar /media/bay/backup/hosts/luxor/lxd/networks .
# rm -rf security/
# cp -ar /media/bay/backup/hosts/luxor/lxd/security .
# cp -ar /media/bay/backup/hosts/luxor/lxd/shmounts/* /var/snap/lxd/common/shmounts/instances/
# cd storage-pools/default/containers-snapshots/
root@luxor:/var/snap/lxd/common/lxd/storage-pools/default/containers-snapshots
# tar -xvf /media/bay/backup/hosts/luxor/lxd/storage-pools/default/snapshots.tar.bz2
```

Restored snapshots (last line above),
and **also restored the entire `containers/` directory from backup**.

Restart the LXD services.

```
# systemctl start snap.lxd.daemon.unix.socket
# systemctl start snap.lxd.daemon.service
```

Attempting `lxc info` returned an error

```
# lxc info
Error: Get "http://unix.socket/1.0": EOF
```

and by checking the log of `snap.lxd.daemon.service` we can see more details:

```
lvl=eror msg="Failed to start the daemon" err="Failed initializing storage pool \"default\": Failed to mount \"/var/lib/snapd/hostfs/var/lib/lxd/storage-pools/default\" on \"/var/snap/lxd/common/lxd/storage-pools/default\" using \"none\": no such file or directory"
```

+ https://discuss.linuxcontainers.org/t/problem-with-lxc-failed-initializing-storage-pool-default/11856/5

This directory is far from empty:
```
root@luxor:/var/snap/lxd/common/lxd/storage-pools/default
# ll /var/snap/lxd/common/lxd/storage-pools/default
total 36K
drwx--x--x  9 root root 4.0K jul 27 19:32 .
drwx--x--x  3 root root 4.0K jul 27 16:26 ..
drwx--x--x 13 root root 4.0K jul 24 06:07 containers
drwx--x--x 13 root root 4.0K jul 25 01:21 containers-snapshots
drwx--x--x  2 root root 4.0K jul 27 16:26 custom
drwx--x--x  2 root root 4.0K jul 27 16:26 custom-snapshots
drwx--x--x  2 root root 4.0K jul 27 16:26 images
drwx--x--x  2 root root 4.0K jul 27 16:26 virtual-machines
drwx--x--x  2 root root 4.0K jul 27 16:26 virtual-machines-snapshots
```

What about:
```
# cat /proc/mounts | grep lxd
/dev/loop1 /snap/lxd/22753 squashfs ro,nodev,relatime,errors=continue 0 0
nsfs /run/snapd/ns/lxd.mnt nsfs rw 0 0
tmpfs /var/snap/lxd/common/ns tmpfs rw,relatime,size=1024k,mode=700,inode64 0 0
nsfs /var/snap/lxd/common/ns/shmounts nsfs rw 0 0
nsfs /var/snap/lxd/common/ns/mntns nsfs rw 0 0
```

The easy fix (possibly) is to simply reboot.
Did that. **Not fixed**, unfortunately.

Same error `Error: Get "http://unix.socket/1.0": EOF` when attempting `lxc info`
or `lxc list`, but systemctl log now lacks any related error messages.

```
taha@luxor:~
$ sudo losetup -a
/dev/loop1: [2130]:27001173 (/var/lib/snapd/snaps/core_13425.snap)
/dev/loop6: [2130]:27004496 (/var/lib/snapd/snaps/snapd_16292.snap)
/dev/loop4: [2130]:27001520 (/var/lib/snapd/snaps/lxd_22753.snap)
/dev/loop2: [2130]:27004575 (/var/lib/snapd/snaps/core20_1581.snap)
/dev/loop0: [2130]:27008638 (/var/lib/snapd/snaps/certbot_2192.snap)
/dev/loop5: [2130]:27003938 (/var/lib/snapd/snaps/snapd_15534.snap)
/dev/loop3: [2130]:27001330 (/var/lib/snapd/snaps/core20_1587.snap)
```

Disaster recovery does not work either
```
$ sudo lxd recover
Error: Get "http://unix.socket/1.0": EOF
```


## Try migrating our snapshots one by one using `lxc import`

Start from a blank slate:
```
snap remove --purge lxd
```
and then reran this role.

Created a tarball of an individual instance's snapshot,
this archive contained the following path `canton/before-luxor-upgrade/backup.yaml`,
and resulted in this poorly document error message:
```
# lxc import /media/bay/backup/hosts/luxor/lxd/storage-pools/default/snapshots/canton.tar.gz
Importing instance: 100% (1.54GB/s)
Error: Backup is missing index.yaml
```

Perhaps the archive needs to have `backup.yaml` in the root of the archive?

+ https://discuss.linuxcontainers.org/t/how-to-move-containers-from-apt-lxd-to-snap-lxd-import-export/4035
+ https://github.com/lxc/lxd/issues/3005

> You’re trying to import a container tarball as a backup tarball, this will not work
> as your hand generated tarball does not contain the index files and extra metadata
> that a real backup would.
> Stéphane Graber, maintainer of linuxcontainers.org

Well, so that's not going to work then.


## Manually migrate container directory...

... and manually edit `backup.yaml` to reflect the new system's LXD path etc.

New `backup.yaml`:
```
container:
  architecture: x86_64
  config:
    image.architecture: amd64
    image.description: ubuntu 20.04 LTS amd64 (release) (20220207)
    image.label: release
    image.os: ubuntu
    image.release: focal
    image.serial: "20220207"
    image.type: squashfs
    image.version: "20.04"
    limits.memory: 2GB
    security.nesting: "false"
    security.privileged: "false"
    volatile.apply_template: create
    volatile.base_image: 017d8572a250a681aec28aa59da770d95db2b79ba156dd590d5f250d0df974ea
    volatile.eth0.name: eth0
    volatile.idmap.base: "0"
    volatile.idmap.next: '[{"Isuid":true,"Isgid":false,"Hostid":100000,"Nsid":0,"Maprange":996},{"Isuid":true,"Isgid":false,"Hostid":996,"Nsid":996,"Maprange":1},{"Isuid":true,"Isgid":false,"Hostid":100997,"Nsid":997,"Maprange":64539},{"Isuid":false,"Isgid":true,"Hostid":100000,"Nsid":0,"Maprange":996},{"Isuid":false,"Isgid":true,"Hostid":996,"Nsid":996,"Maprange":1},{"Isuid":false,"Isgid":true,"Hostid":100997,"Nsid":997,"Maprange":64539}]'
    volatile.last_state.idmap: '[{"Isuid":true,"Isgid":false,"Hostid":100000,"Nsid":0,"Maprange":996},{"Isuid":true,"Isgid":false,"Hostid":996,"Nsid":996,"Maprange":1},{"Isuid":true,"Isgid":false,"Hostid":100997,"Nsid":997,"Maprange":64539},{"Isuid":false,"Isgid":true,"Hostid":100000,"Nsid":0,"Maprange":996},{"Isuid":false,"Isgid":true,"Hostid":996,"Nsid":996,"Maprange":1},{"Isuid":false,"Isgid":true,"Hostid":100997,"Nsid":997,"Maprange":64539}]'
    volatile.last_state.power: RUNNING
  devices:
    eth0:
      hwaddr: 00:16:3e:6c:59:86
      nictype: bridged
      parent: br0
      type: nic
    root:
      path: /
      pool: default
      size: 5GB
      type: disk
  ephemeral: false
  profiles:
  - piratebox
  stateful: false
  description: ""
  created_at: 2022-02-23T21:31:57+01:00
  expanded_config:
    boot.autostart: "true"
    boot.autostart.delay: "5"
    boot.autostart.priority: "10"
    image.architecture: amd64
    image.description: ubuntu 20.04 LTS amd64 (release) (20220207)
    image.label: release
    image.os: ubuntu
    image.release: focal
    image.serial: "20220207"
    image.type: squashfs
    image.version: "20.04"
    limits.memory: 2GB
    raw.idmap: |
      uid 996 996
      gid 996 996
    security.nesting: "false"
    security.privileged: "false"
    user.user-data: |
      #cloud-config
      users:
        - name: taha
          groups: sudo
          lock_passwd: true
          shell: /bin/bash
          sudo: ['ALL=(ALL) NOPASSWD:ALL']
          ssh_authorized_keys:
            - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHEIjxElkrYvm88/O7Z7JIjQFvr3kWnhup9yEJPQFMIO taha@asks2
    volatile.apply_template: create
    volatile.base_image: 017d8572a250a681aec28aa59da770d95db2b79ba156dd590d5f250d0df974ea
    volatile.eth0.name: eth0
    volatile.idmap.base: "0"
    volatile.idmap.next: '[{"Isuid":true,"Isgid":false,"Hostid":100000,"Nsid":0,"Maprange":996},{"Isuid":true,"Isgid":false,"Hostid":996,"Nsid":996,"Maprange":1},{"Isuid":true,"Isgid":false,"Hostid":100997,"Nsid":997,"Maprange":64539},{"Isuid":false,"Isgid":true,"Hostid":100000,"Nsid":0,"Maprange":996},{"Isuid":false,"Isgid":true,"Hostid":996,"Nsid":996,"Maprange":1},{"Isuid":false,"Isgid":true,"Hostid":100997,"Nsid":997,"Maprange":64539}]'
    volatile.last_state.idmap: '[{"Isuid":true,"Isgid":false,"Hostid":100000,"Nsid":0,"Maprange":996},{"Isuid":true,"Isgid":false,"Hostid":996,"Nsid":996,"Maprange":1},{"Isuid":true,"Isgid":false,"Hostid":100997,"Nsid":997,"Maprange":64539},{"Isuid":false,"Isgid":true,"Hostid":100000,"Nsid":0,"Maprange":996},{"Isuid":false,"Isgid":true,"Hostid":996,"Nsid":996,"Maprange":1},{"Isuid":false,"Isgid":true,"Hostid":100997,"Nsid":997,"Maprange":64539}]'
    volatile.last_state.power: RUNNING
  expanded_devices:
    bay_collections:
      path: /media/bay/collections
      source: /media/bay/collections
      type: disk
    eth0:
      hwaddr: 00:16:3e:6c:59:86
      nictype: bridged
      parent: br0
      type: nic
    root:
      path: /
      pool: default
      size: 5GB
      type: disk
  name: canton
  status: Stopped
  status_code: 102
  last_used_at: 2022-07-19T05:44:22.531531538+02:00
  location: none
  type: container
  project: default
snapshots: []
pool:
  config:
    source: /var/snap/lxd/common/lxd/storage-pools/default
  description: ""
  name: default
  driver: dir
  used_by: []
  status: Created
  locations:
  - none
volume:
  config: {}
  description: ""
  name: canton
  type: container
  used_by: []
  location: none
  content_type: filesystem
```

Starting the container fails:

```
# lxc start canton
Error: Failed to start device "eth0": Parent device "br0" doesn't exist
```

The log is useless:
```
# lxc info --show-log canton
Name: canton
Location: none
Remote: unix://
Architecture: x86_64
Created: 2022/07/28 00:39 CEST
Status: Stopped
Type: container
Profiles: piratebox

Log:
```

Our network devices:
```
# ip -br link
lo               UNKNOWN        00:00:00:00:00:00 <LOOPBACK,UP,LOWER_UP>
enp0s31f6        UP             d0:50:99:c2:8a:bf <BROADCAST,MULTICAST,UP,LOWER_UP>
enp2s0           DOWN           d0:50:99:c2:8a:c0 <NO-CARRIER,BROADCAST,MULTICAST,UP>
virbr0           DOWN           52:54:00:07:28:8a <NO-CARRIER,BROADCAST,MULTICAST,UP>
lxdbr0           DOWN           00:16:3e:f4:9c:2b <NO-CARRIER,BROADCAST,MULTICAST,UP>
```

On a whim, changed all instances of `parent: br0` to `parent: lxdbr0` in
`backup.yaml`.
The error did not change at all: `Parent device "br0" doesn't exist`.

Is something else setting `br0`? The profile perhaps? Where is that defined?

```
# find /var/snap/lxd/common/lxd -type f -exec grep -irnl --color "piratebox" {} +
/var/snap/lxd/common/lxd/database/global/open-1
/var/snap/lxd/common/lxd/storage-pools/default/containers/canton/backup.yaml
# find /var/snap/lxd/common/lxd -type f -name "*piratebox*"
```
Nada.

Here it is!
```
# lxc config show canton
architecture: x86_64
config:
  image.architecture: amd64
  image.description: ubuntu 20.04 LTS amd64 (release) (20220711)
  image.label: release
  image.os: ubuntu
  image.release: focal
  image.serial: "20220711"
  image.type: squashfs
  image.version: "20.04"
  limits.memory: 2GB
  security.nesting: "false"
  security.privileged: "false"
  volatile.apply_template: create
  volatile.base_image: e9589b6e9c886888b3df98aee0f0e16c5805383418b3563cd8845220f43b40ff
  volatile.eth0.name: eth0
  volatile.idmap.base: "0"
  volatile.idmap.current: '[{"Isuid":true,"Isgid":false,"Hostid":1000000,"Nsid":0,"Maprange":996},{"Isuid":true,"Isgid":false,"Hostid":996,"Nsid":996,"Maprange":1},{"Isuid":true,"Isgid":false,"Hostid":1000997,"Nsid":997,"Maprange":999999003},{"Isuid":false,"Isgid":true,"Hostid":1000000,"Nsid":0,"Maprange":996},{"Isuid":false,"Isgid":true,"Hostid":996,"Nsid":996,"Maprange":1},{"Isuid":false,"Isgid":true,"Hostid":1000997,"Nsid":997,"Maprange":999999003}]'
  volatile.idmap.next: '[{"Isuid":true,"Isgid":false,"Hostid":1000000,"Nsid":0,"Maprange":996},{"Isuid":true,"Isgid":false,"Hostid":996,"Nsid":996,"Maprange":1},{"Isuid":true,"Isgid":false,"Hostid":1000997,"Nsid":997,"Maprange":999999003},{"Isuid":false,"Isgid":true,"Hostid":1000000,"Nsid":0,"Maprange":996},{"Isuid":false,"Isgid":true,"Hostid":996,"Nsid":996,"Maprange":1},{"Isuid":false,"Isgid":true,"Hostid":1000997,"Nsid":997,"Maprange":999999003}]'
  volatile.last_state.idmap: '[]'
  volatile.uuid: e97b88d5-dd39-4fbe-911b-ef6cedc3e25c
devices:
  eth0:
    hwaddr: 00:16:3e:6c:59:86
    nictype: bridged
    parent: br0
    type: nic
  root:
    path: /
    pool: default
    size: 5GB
    type: disk
ephemeral: false
profiles:
- piratebox
stateful: false
description: ""
```

Also, I realised this is entirely my fault. On `luxor` we did indeed have a bridge
adapter `br0`, which was used for all KVM hosts and some containers, such as `canton`,
that get their DHCP leases directly from our LAN's router.

Ok, created the bridge adapter `br0` (see role `network-manager` and luxor's
host vars in playbook).

Ok, promising so far:
```
root@luxor:~
# lxc start canton
root@luxor:~
# lxc list
+---------+---------+----------------------+------+-----------+-----------+
|  NAME   |  STATE  |         IPV4         | IPV6 |   TYPE    | SNAPSHOTS |
+---------+---------+----------------------+------+-----------+-----------+
| antioch | STOPPED |                      |      | CONTAINER | 0         |
+---------+---------+----------------------+------+-----------+-----------+
| beijing | STOPPED |                      |      | CONTAINER | 0         |
+---------+---------+----------------------+------+-----------+-----------+
| bilbeis | STOPPED |                      |      | CONTAINER | 0         |
+---------+---------+----------------------+------+-----------+-----------+
| canton  | RUNNING | 192.168.1.105 (eth0) |      | CONTAINER | 0         |
+---------+---------+----------------------+------+-----------+-----------+
| hunan   | STOPPED |                      |      | CONTAINER | 0         |
+---------+---------+----------------------+------+-----------+-----------+
| karachi | STOPPED |                      |      | CONTAINER | 0         |
+---------+---------+----------------------+------+-----------+-----------+
| lahore  | STOPPED |                      |      | CONTAINER | 0         |
+---------+---------+----------------------+------+-----------+-----------+
| muscat  | STOPPED |                      |      | CONTAINER | 0         |
+---------+---------+----------------------+------+-----------+-----------+
| taipei  | STOPPED |                      |      | CONTAINER | 0         |
+---------+---------+----------------------+------+-----------+-----------+
| xian    | STOPPED |                      |      | CONTAINER | 0         |
+---------+---------+----------------------+------+-----------+-----------+
```

Not possible to SSH into the host. Weird.

Get shell access from the LXD server like this:
```
taha@luxor:~
$ lxc exec canton -- /bin/bash
```

Turns out the entire filesystem of the running container is owned by `root`,
including our user's home directory!

```
root@canton:~
# chown -R taha:taha /home/taha
```

Can now SSH to the container. OK.

But if the entire filesystem is owned by root, that means so are the various
working directories and other stuff of different services that should be
running as their own user accounts.

Yep, all those systemd services are broken in some way or other.
Very likely caused by this filesystem ownership mishap.


## Manually migrate container directory, but without losing ownerships

In our backup, it looks like this

+ files that were owned by `root` have UID/GID 100000
+ files that were owned by `taha` have UID/GID 101000

these id's are not mapped to anything on `luxor`, and thus presented numerically.
Other id's correspond to existing users (such as `www-data`, or `deluge`, etc.)
and are thus shown as such.

I think it would be a good idea to

+ convert `100000` to `root`, and likewise `101000` to `taha` in the backup
  directory itself. I imagine a snazzy `find` construct should be able to do it.
+ when restoring (copying) the backup into place on the new LXD server, take
  care to use a command that respects ownership (also symlinks).

+ https://www.cyberciti.biz/faq/linux-change-user-group-uid-gid-for-all-owned-files/

Here's `karachi`:
```
root@luxor:/media/bay/backup/hosts/luxor/lxd/storage-pools/default/containers/karachi/rootfs
# find . -user 100000 -exec chown -h root {} \;
# find . -group 100000 -exec chgrp -h root {} \;

# find . -user 101000 -exec chown -h taha {} \;
# find . -group 101000 -exec chgrp -h taha {} \;
```
(the `-h` flag was necessary to avoid a lot of warnings about `cannot dereference ...`).
Expect each command to run for several minutes (no output is produced).

Wipe the existing (empty) container on `luxor`:
```
root@luxor:/var/snap/lxd/common/lxd/storage-pools/default/containers
# rm -rf karachi/
```

Ok, let's restore from backup (taking care to preserve symllinks `-l`, permissions `-p`,
times `-t`, group ownership `-g`, user ownership `-o`, special files and devices `-D`,
executability `-E`, and ACLs `-A`):
```
root@luxor:/
# rsync -rlptgoDEAXv /media/bay/backup/hosts/luxor/lxd/storage-pools/default/containers/karachi /var/snap/lxd/common/lxd/storage-pools/default/containers/
```

Minimally edit the `backup.yaml`:

+ change to `pool.config.source: /var/snap/lxd/common/lxd/storage-pools/default`
+ remove any lines about snapshots

Start the container:
```
# lxc start karachi
# lxc list --fast
+---------+---------+--------------+----------------------+------------------+-----------+
|  NAME   |  STATE  | ARCHITECTURE |      CREATED AT      |     PROFILES     |   TYPE    |
+---------+---------+--------------+----------------------+------------------+-----------+
| antioch | STOPPED | x86_64       | 2022/07/27 22:41 UTC | calibreweb       | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| beijing | STOPPED | x86_64       | 2022/07/27 22:39 UTC | default          | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| bilbeis | STOPPED | x86_64       | 2022/07/27 22:39 UTC | dns-server       | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| canton  | RUNNING | x86_64       | 2022/07/27 22:39 UTC | piratebox        | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| hunan   | STOPPED | x86_64       | 2022/07/27 22:40 UTC | rstudio-server   | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| karachi | RUNNING | x86_64       | 2022/07/27 22:40 UTC | nextcloud-server | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| lahore  | STOPPED | x86_64       | 2022/07/27 22:40 UTC | nextcloud-server | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| muscat  | STOPPED | x86_64       | 2022/07/27 22:41 UTC | default          | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| taipei  | STOPPED | x86_64       | 2022/07/27 22:41 UTC | archivebox       | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
| xian    | STOPPED | x86_64       | 2022/07/27 22:41 UTC | rstudio-server   | CONTAINER |
+---------+---------+--------------+----------------------+------------------+-----------+
```

SSH to container works.
All services are broken. Seems related to database.

Look at this:
```
taha@karachi:~
$ sudo systemctl status mariadb.service
sudo: /usr/bin/sudo must be owned by uid 0 and have the setuid bit set
```

Add `-X` to rsync flags and restore again.
Same error.

I think my backup actually lacks at least the setuid bit ("extended attributes").
Making it useless.

Let's copy straight from the mounted image we made of the entire disk.
All file attributes can be expected to be intact there.
Also, let's not mess about with ownership on this mount.
```
root@asks2:/media/taha/c26b6f17-93a0-4713-aeb0-4931cd3a9ff8/var/lib/lxd/storage-pools/default/containers
# rsync -rlptgoDEAXv karachi luxor-root:/var/snap/lxd/common/lxd/storage-pools/default/containers/
```

Extended attributes now look good. Still using uid `100000` etc. but let's leave
it like that and see what happens.

Started the container.
Well, not possible to SSH into it.

Opening a shell into the container:
```
root@luxor:/
# lxc exec karachi -- /bin/bash
```

Maybe it's safer to change owner/group from inside the container?
```
root@karachi:~
find / -user 100000 -exec chown -h root {} \;
find / -group 100000 -exec chgrp -h root {} \;
find / -user 101000 -exec chown -h taha {} \;
find / -group 101000 -exec chgrp -h taha {} \;
```

No, doesn't look so good. Bunch of `permission denied` errors, it appears caused
by a lot of files that are owned by `nobody:nogroup` (and they should not be).

It seems we should adjust owner/group inside the `/var/snap/lxd` tree then.
```
root@luxor:/var/snap/lxd/common/lxd/storage-pools/default/containers
find karachi/rootfs -user 100000 -exec chown -h root {} \;
find karachi/rootfs -group 100000 -exec chgrp -h root {} \;
find karachi/rootfs -user 101000 -exec chown -h taha {} \;
find karachi/rootfs -group 101000 -exec chgrp -h taha {} \;
```

Let's start the container.
Wait a minute. Does either the `chown` or `chgrp` commands strip any setuid bits?
Yes, it does!

+ https://unix.stackexchange.com/questions/53665/chown-removes-setuid-bit-bug-or-feature
+ https://stackpointer.io/unix/unix-linux-find-setuid-files/532/
+ https://www.tecmint.com/how-to-find-files-with-suid-and-sgid-permissions-in-linux/

I'm running these commands as root, and still the setuid/setgid bits are stripped.

Find files with SUID set:
```
root@asks2:/media/taha/c26b6f17-93a0-4713-aeb0-4931cd3a9ff8/var/lib/lxd/storage-pools/default/containers
# find karachi/rootfs -perm -4000 -exec ls -ld {} \;
```

Find files with SGID set:
```
root@asks2:/media/taha/c26b6f17-93a0-4713-aeb0-4931cd3a9ff8/var/lib/lxd/storage-pools/default/containers
# find karachi/rootfs -perm -2000 -exec ls -ld {} \;
```

**To set the SUID `chmod u+s <file>`, and likewise to set the SGID `chmod g+s <file>`.**

Unfortunately, we will need to do this in two steps.
First, on the backup, list all files with SUID and SGID bit set, respectively:
```
root@asks2:/media/taha/c26b6f17-93a0-4713-aeb0-4931cd3a9ff8/var/lib/lxd/storage-pools/default/containers/karachi/rootfs
# find . -perm -4000 -exec ls -d {} \;
./usr/bin/pkexec
./usr/bin/passwd
./usr/bin/gpasswd
./usr/bin/chfn
./usr/bin/mount
./usr/bin/umount
./usr/bin/sudo
./usr/bin/at
./usr/bin/fusermount
./usr/bin/su
./usr/bin/chsh
./usr/bin/newgrp
./usr/sbin/mount.cifs
./usr/sbin/mount.nfs
./usr/lib/dbus-1.0/dbus-daemon-launch-helper
./usr/lib/openssh/ssh-keysign
./usr/lib/eject/dmcrypt-get-device
./usr/lib/mysql/plugin/auth_pam_tool_dir/auth_pam_tool
./usr/lib/policykit-1/polkit-agent-helper-1
./usr/lib/snapd/snap-confine

root@asks2:/media/taha/c26b6f17-93a0-4713-aeb0-4931cd3a9ff8/var/lib/lxd/storage-pools/default/containers/karachi/rootfs
# find . -perm -2000 -exec ls -d {} \;
./usr/local/lib/python3.8
./usr/local/lib/python3.8/dist-packages
./usr/local/lib/python3.8/dist-packages/future-0.18.2.dist-info
./usr/local/lib/python3.8/dist-packages/wheel-0.37.1.dist-info
./usr/local/lib/python3.8/dist-packages/setuptools
./usr/local/lib/python3.8/dist-packages/setuptools/__pycache__
./usr/local/lib/python3.8/dist-packages/setuptools/_distutils
./usr/local/lib/python3.8/dist-packages/setuptools/_distutils/__pycache__
./usr/local/lib/python3.8/dist-packages/setuptools/_distutils/command
./usr/local/lib/python3.8/dist-packages/setuptools/_distutils/command/__pycache__
./usr/local/lib/python3.8/dist-packages/setuptools/command
./usr/local/lib/python3.8/dist-packages/setuptools/command/__pycache__
./usr/local/lib/python3.8/dist-packages/setuptools/extern
./usr/local/lib/python3.8/dist-packages/setuptools/extern/__pycache__
./usr/local/lib/python3.8/dist-packages/setuptools/_vendor
./usr/local/lib/python3.8/dist-packages/setuptools/_vendor/jaraco
./usr/local/lib/python3.8/dist-packages/setuptools/_vendor/jaraco/text
./usr/local/lib/python3.8/dist-packages/setuptools/_vendor/jaraco/text/__pycache__
./usr/local/lib/python3.8/dist-packages/setuptools/_vendor/jaraco/__pycache__
./usr/local/lib/python3.8/dist-packages/setuptools/_vendor/__pycache__
./usr/local/lib/python3.8/dist-packages/setuptools/_vendor/packaging
./usr/local/lib/python3.8/dist-packages/setuptools/_vendor/packaging/__pycache__
./usr/local/lib/python3.8/dist-packages/setuptools/_vendor/importlib_resources
./usr/local/lib/python3.8/dist-packages/setuptools/_vendor/importlib_resources/__pycache__
./usr/local/lib/python3.8/dist-packages/setuptools/_vendor/more_itertools
./usr/local/lib/python3.8/dist-packages/setuptools/_vendor/more_itertools/__pycache__
./usr/local/lib/python3.8/dist-packages/setuptools/_vendor/importlib_metadata
./usr/local/lib/python3.8/dist-packages/setuptools/_vendor/importlib_metadata/__pycache__
./usr/local/lib/python3.8/dist-packages/pyparsing
./usr/local/lib/python3.8/dist-packages/pyparsing/diagram
./usr/local/lib/python3.8/dist-packages/pyparsing/diagram/__pycache__
./usr/local/lib/python3.8/dist-packages/pyparsing/__pycache__
./usr/local/lib/python3.8/dist-packages/libpasteurize
./usr/local/lib/python3.8/dist-packages/libpasteurize/fixes
./usr/local/lib/python3.8/dist-packages/libpasteurize/fixes/__pycache__
./usr/local/lib/python3.8/dist-packages/libpasteurize/__pycache__
./usr/local/lib/python3.8/dist-packages/pymysql
./usr/local/lib/python3.8/dist-packages/pymysql/__pycache__
./usr/local/lib/python3.8/dist-packages/pymysql/constants
./usr/local/lib/python3.8/dist-packages/pymysql/constants/__pycache__
./usr/local/lib/python3.8/dist-packages/__pycache__
./usr/local/lib/python3.8/dist-packages/pkg_resources
./usr/local/lib/python3.8/dist-packages/pkg_resources/__pycache__
./usr/local/lib/python3.8/dist-packages/pkg_resources/extern
./usr/local/lib/python3.8/dist-packages/pkg_resources/extern/__pycache__
./usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor
./usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/jaraco
./usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/jaraco/text
./usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/jaraco/text/__pycache__
./usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/jaraco/__pycache__
./usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/__pycache__
./usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/packaging
./usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/packaging/__pycache__
./usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/importlib_resources
./usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/importlib_resources/__pycache__
./usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/more_itertools
./usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/more_itertools/__pycache__
./usr/local/lib/python3.8/dist-packages/pyparsing-3.0.7.dist-info
./usr/local/lib/python3.8/dist-packages/psutil-5.9.0.dist-info
./usr/local/lib/python3.8/dist-packages/setuptools-60.9.3.dist-info
./usr/local/lib/python3.8/dist-packages/packaging
./usr/local/lib/python3.8/dist-packages/packaging/__pycache__
./usr/local/lib/python3.8/dist-packages/_distutils_hack
./usr/local/lib/python3.8/dist-packages/_distutils_hack/__pycache__
./usr/local/lib/python3.8/dist-packages/libfuturize
./usr/local/lib/python3.8/dist-packages/libfuturize/fixes
./usr/local/lib/python3.8/dist-packages/libfuturize/fixes/__pycache__
./usr/local/lib/python3.8/dist-packages/libfuturize/__pycache__
./usr/local/lib/python3.8/dist-packages/bottle-0.12.19.dist-info
./usr/local/lib/python3.8/dist-packages/Glances-3.2.4.2.dist-info
./usr/local/lib/python3.8/dist-packages/wheel
./usr/local/lib/python3.8/dist-packages/wheel/__pycache__
./usr/local/lib/python3.8/dist-packages/wheel/cli
./usr/local/lib/python3.8/dist-packages/wheel/cli/__pycache__
./usr/local/lib/python3.8/dist-packages/wheel/vendored
./usr/local/lib/python3.8/dist-packages/wheel/vendored/__pycache__
./usr/local/lib/python3.8/dist-packages/wheel/vendored/packaging
./usr/local/lib/python3.8/dist-packages/wheel/vendored/packaging/__pycache__
./usr/local/lib/python3.8/dist-packages/future
./usr/local/lib/python3.8/dist-packages/future/utils
./usr/local/lib/python3.8/dist-packages/future/utils/__pycache__
./usr/local/lib/python3.8/dist-packages/future/backports
./usr/local/lib/python3.8/dist-packages/future/backports/__pycache__
./usr/local/lib/python3.8/dist-packages/future/backports/xmlrpc
./usr/local/lib/python3.8/dist-packages/future/backports/xmlrpc/__pycache__
./usr/local/lib/python3.8/dist-packages/future/backports/urllib
./usr/local/lib/python3.8/dist-packages/future/backports/urllib/__pycache__
./usr/local/lib/python3.8/dist-packages/future/backports/html
./usr/local/lib/python3.8/dist-packages/future/backports/html/__pycache__
./usr/local/lib/python3.8/dist-packages/future/backports/email
./usr/local/lib/python3.8/dist-packages/future/backports/email/__pycache__
./usr/local/lib/python3.8/dist-packages/future/backports/email/mime
./usr/local/lib/python3.8/dist-packages/future/backports/email/mime/__pycache__
./usr/local/lib/python3.8/dist-packages/future/backports/test
./usr/local/lib/python3.8/dist-packages/future/backports/test/__pycache__
./usr/local/lib/python3.8/dist-packages/future/backports/http
./usr/local/lib/python3.8/dist-packages/future/backports/http/__pycache__
./usr/local/lib/python3.8/dist-packages/future/__pycache__
./usr/local/lib/python3.8/dist-packages/future/moves
./usr/local/lib/python3.8/dist-packages/future/moves/__pycache__
./usr/local/lib/python3.8/dist-packages/future/moves/xmlrpc
./usr/local/lib/python3.8/dist-packages/future/moves/xmlrpc/__pycache__
./usr/local/lib/python3.8/dist-packages/future/moves/urllib
./usr/local/lib/python3.8/dist-packages/future/moves/urllib/__pycache__
./usr/local/lib/python3.8/dist-packages/future/moves/html
./usr/local/lib/python3.8/dist-packages/future/moves/html/__pycache__
./usr/local/lib/python3.8/dist-packages/future/moves/test
./usr/local/lib/python3.8/dist-packages/future/moves/test/__pycache__
./usr/local/lib/python3.8/dist-packages/future/moves/http
./usr/local/lib/python3.8/dist-packages/future/moves/http/__pycache__
./usr/local/lib/python3.8/dist-packages/future/moves/tkinter
./usr/local/lib/python3.8/dist-packages/future/moves/tkinter/__pycache__
./usr/local/lib/python3.8/dist-packages/future/moves/dbm
./usr/local/lib/python3.8/dist-packages/future/moves/dbm/__pycache__
./usr/local/lib/python3.8/dist-packages/future/tests
./usr/local/lib/python3.8/dist-packages/future/tests/__pycache__
./usr/local/lib/python3.8/dist-packages/future/standard_library
./usr/local/lib/python3.8/dist-packages/future/standard_library/__pycache__
./usr/local/lib/python3.8/dist-packages/future/builtins
./usr/local/lib/python3.8/dist-packages/future/builtins/__pycache__
./usr/local/lib/python3.8/dist-packages/future/types
./usr/local/lib/python3.8/dist-packages/future/types/__pycache__
./usr/local/lib/python3.8/dist-packages/defusedxml
./usr/local/lib/python3.8/dist-packages/defusedxml/__pycache__
./usr/local/lib/python3.8/dist-packages/defusedxml-0.7.1.dist-info
./usr/local/lib/python3.8/dist-packages/psutil
./usr/local/lib/python3.8/dist-packages/psutil/__pycache__
./usr/local/lib/python3.8/dist-packages/psutil/tests
./usr/local/lib/python3.8/dist-packages/psutil/tests/__pycache__
./usr/local/lib/python3.8/dist-packages/glances
./usr/local/lib/python3.8/dist-packages/glances/outputs
./usr/local/lib/python3.8/dist-packages/glances/outputs/static
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/css
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/images
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/templates
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/public
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-irq
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-uptime
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-load
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/help
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-gpu
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-quicklook
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-diskio
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-alert
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-docker
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-system
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-folders
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-process
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-memswap
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-amps
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-cloud
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-ports
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-fs
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-raid
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-mem
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-sensors
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-wifi
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-network
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-percpu
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-connections
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/glances
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-ip
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-processlist
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-cpu
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-mem-more
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-processcount
./usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/services
./usr/local/lib/python3.8/dist-packages/glances/outputs/__pycache__
./usr/local/lib/python3.8/dist-packages/glances/amps
./usr/local/lib/python3.8/dist-packages/glances/amps/__pycache__
./usr/local/lib/python3.8/dist-packages/glances/__pycache__
./usr/local/lib/python3.8/dist-packages/glances/plugins
./usr/local/lib/python3.8/dist-packages/glances/plugins/__pycache__
./usr/local/lib/python3.8/dist-packages/glances/plugins/sensors
./usr/local/lib/python3.8/dist-packages/glances/plugins/sensors/__pycache__
./usr/local/lib/python3.8/dist-packages/glances/exports
./usr/local/lib/python3.8/dist-packages/glances/exports/__pycache__
./usr/local/lib/python3.8/dist-packages/PyMySQL-1.0.2.dist-info
./usr/local/lib/python3.8/dist-packages/past
./usr/local/lib/python3.8/dist-packages/past/translation
./usr/local/lib/python3.8/dist-packages/past/translation/__pycache__
./usr/local/lib/python3.8/dist-packages/past/utils
./usr/local/lib/python3.8/dist-packages/past/utils/__pycache__
./usr/local/lib/python3.8/dist-packages/past/__pycache__
./usr/local/lib/python3.8/dist-packages/past/builtins
./usr/local/lib/python3.8/dist-packages/past/builtins/__pycache__
./usr/local/lib/python3.8/dist-packages/past/types
./usr/local/lib/python3.8/dist-packages/past/types/__pycache__
./usr/local/lib/python3.8/dist-packages/packaging-21.3.dist-info
./usr/local/lib/python2.7
./usr/local/lib/python2.7/site-packages
./usr/local/lib/python2.7/dist-packages
./usr/local/share/fonts
./usr/bin/crontab
./usr/bin/wall
./usr/bin/mlock
./usr/bin/expiry
./usr/bin/at
./usr/bin/bsd-write
./usr/bin/chage
./usr/bin/ssh-agent
./usr/sbin/unix_chkpwd
./usr/sbin/pam_extrausers_chkpwd
./usr/lib/x86_64-linux-gnu/utempter/utempter
./var/local
./var/mail
./var/log/journal
./var/log/journal/d29f62c14a65486eb2d5bc189b4adbf3
./var/log/mysql
./var/log/redis
```


On the container, `lxc exec karachi -- /bin/bash`:
```
root@karachi:~
chmod u+s /usr/bin/pkexec
chmod u+s /usr/bin/passwd
chmod u+s /usr/bin/gpasswd
chmod u+s /usr/bin/chfn
chmod u+s /usr/bin/mount
chmod u+s /usr/bin/umount
chmod u+s /usr/bin/sudo
chmod u+s /usr/bin/at
chmod u+s /usr/bin/fusermount
chmod u+s /usr/bin/su
chmod u+s /usr/bin/chsh
chmod u+s /usr/bin/newgrp
chmod u+s /usr/sbin/mount.cifs
chmod u+s /usr/sbin/mount.nfs
chmod u+s /usr/lib/dbus-1.0/dbus-daemon-launch-helper
chmod u+s /usr/lib/openssh/ssh-keysign
chmod u+s /usr/lib/eject/dmcrypt-get-device
chmod u+s /usr/lib/mysql/plugin/auth_pam_tool_dir/auth_pam_tool
chmod u+s /usr/lib/policykit-1/polkit-agent-helper-1
chmod u+s /usr/lib/snapd/snap-confine

chmod g+s /usr/local/lib/python3.8
chmod g+s /usr/local/lib/python3.8/dist-packages
chmod g+s /usr/local/lib/python3.8/dist-packages/future-0.18.2.dist-info
chmod g+s /usr/local/lib/python3.8/dist-packages/wheel-0.37.1.dist-info
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/_distutils
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/_distutils/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/_distutils/command
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/_distutils/command/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/command
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/command/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/extern
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/extern/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/_vendor
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/_vendor/jaraco
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/_vendor/jaraco/text
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/_vendor/jaraco/text/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/_vendor/jaraco/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/_vendor/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/_vendor/packaging
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/_vendor/packaging/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/_vendor/importlib_resources
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/_vendor/importlib_resources/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/_vendor/more_itertools
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/_vendor/more_itertools/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/_vendor/importlib_metadata
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools/_vendor/importlib_metadata/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/pyparsing
chmod g+s /usr/local/lib/python3.8/dist-packages/pyparsing/diagram
chmod g+s /usr/local/lib/python3.8/dist-packages/pyparsing/diagram/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/pyparsing/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/libpasteurize
chmod g+s /usr/local/lib/python3.8/dist-packages/libpasteurize/fixes
chmod g+s /usr/local/lib/python3.8/dist-packages/libpasteurize/fixes/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/libpasteurize/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/pymysql
chmod g+s /usr/local/lib/python3.8/dist-packages/pymysql/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/pymysql/constants
chmod g+s /usr/local/lib/python3.8/dist-packages/pymysql/constants/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/pkg_resources
chmod g+s /usr/local/lib/python3.8/dist-packages/pkg_resources/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/pkg_resources/extern
chmod g+s /usr/local/lib/python3.8/dist-packages/pkg_resources/extern/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor
chmod g+s /usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/jaraco
chmod g+s /usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/jaraco/text
chmod g+s /usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/jaraco/text/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/jaraco/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/packaging
chmod g+s /usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/packaging/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/importlib_resources
chmod g+s /usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/importlib_resources/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/more_itertools
chmod g+s /usr/local/lib/python3.8/dist-packages/pkg_resources/_vendor/more_itertools/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/pyparsing-3.0.7.dist-info
chmod g+s /usr/local/lib/python3.8/dist-packages/psutil-5.9.0.dist-info
chmod g+s /usr/local/lib/python3.8/dist-packages/setuptools-60.9.3.dist-info
chmod g+s /usr/local/lib/python3.8/dist-packages/packaging
chmod g+s /usr/local/lib/python3.8/dist-packages/packaging/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/_distutils_hack
chmod g+s /usr/local/lib/python3.8/dist-packages/_distutils_hack/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/libfuturize
chmod g+s /usr/local/lib/python3.8/dist-packages/libfuturize/fixes
chmod g+s /usr/local/lib/python3.8/dist-packages/libfuturize/fixes/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/libfuturize/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/bottle-0.12.19.dist-info
chmod g+s /usr/local/lib/python3.8/dist-packages/Glances-3.2.4.2.dist-info
chmod g+s /usr/local/lib/python3.8/dist-packages/wheel
chmod g+s /usr/local/lib/python3.8/dist-packages/wheel/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/wheel/cli
chmod g+s /usr/local/lib/python3.8/dist-packages/wheel/cli/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/wheel/vendored
chmod g+s /usr/local/lib/python3.8/dist-packages/wheel/vendored/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/wheel/vendored/packaging
chmod g+s /usr/local/lib/python3.8/dist-packages/wheel/vendored/packaging/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future
chmod g+s /usr/local/lib/python3.8/dist-packages/future/utils
chmod g+s /usr/local/lib/python3.8/dist-packages/future/utils/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/backports
chmod g+s /usr/local/lib/python3.8/dist-packages/future/backports/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/backports/xmlrpc
chmod g+s /usr/local/lib/python3.8/dist-packages/future/backports/xmlrpc/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/backports/urllib
chmod g+s /usr/local/lib/python3.8/dist-packages/future/backports/urllib/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/backports/html
chmod g+s /usr/local/lib/python3.8/dist-packages/future/backports/html/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/backports/email
chmod g+s /usr/local/lib/python3.8/dist-packages/future/backports/email/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/backports/email/mime
chmod g+s /usr/local/lib/python3.8/dist-packages/future/backports/email/mime/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/backports/test
chmod g+s /usr/local/lib/python3.8/dist-packages/future/backports/test/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/backports/http
chmod g+s /usr/local/lib/python3.8/dist-packages/future/backports/http/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/moves
chmod g+s /usr/local/lib/python3.8/dist-packages/future/moves/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/moves/xmlrpc
chmod g+s /usr/local/lib/python3.8/dist-packages/future/moves/xmlrpc/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/moves/urllib
chmod g+s /usr/local/lib/python3.8/dist-packages/future/moves/urllib/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/moves/html
chmod g+s /usr/local/lib/python3.8/dist-packages/future/moves/html/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/moves/test
chmod g+s /usr/local/lib/python3.8/dist-packages/future/moves/test/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/moves/http
chmod g+s /usr/local/lib/python3.8/dist-packages/future/moves/http/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/moves/tkinter
chmod g+s /usr/local/lib/python3.8/dist-packages/future/moves/tkinter/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/moves/dbm
chmod g+s /usr/local/lib/python3.8/dist-packages/future/moves/dbm/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/tests
chmod g+s /usr/local/lib/python3.8/dist-packages/future/tests/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/standard_library
chmod g+s /usr/local/lib/python3.8/dist-packages/future/standard_library/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/builtins
chmod g+s /usr/local/lib/python3.8/dist-packages/future/builtins/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/future/types
chmod g+s /usr/local/lib/python3.8/dist-packages/future/types/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/defusedxml
chmod g+s /usr/local/lib/python3.8/dist-packages/defusedxml/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/defusedxml-0.7.1.dist-info
chmod g+s /usr/local/lib/python3.8/dist-packages/psutil
chmod g+s /usr/local/lib/python3.8/dist-packages/psutil/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/psutil/tests
chmod g+s /usr/local/lib/python3.8/dist-packages/psutil/tests/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/glances
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/css
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/images
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/templates
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/public
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-irq
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-uptime
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-load
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/help
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-gpu
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-quicklook
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-diskio
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-alert
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-docker
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-system
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-folders
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-process
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-memswap
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-amps
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-cloud
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-ports
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-fs
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-raid
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-mem
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-sensors
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-wifi
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-network
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-percpu
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-connections
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/glances
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-ip
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-processlist
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-cpu
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-mem-more
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/components/plugin-processcount
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/static/js/services
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/outputs/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/amps
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/amps/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/plugins
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/plugins/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/plugins/sensors
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/plugins/sensors/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/exports
chmod g+s /usr/local/lib/python3.8/dist-packages/glances/exports/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/PyMySQL-1.0.2.dist-info
chmod g+s /usr/local/lib/python3.8/dist-packages/past
chmod g+s /usr/local/lib/python3.8/dist-packages/past/translation
chmod g+s /usr/local/lib/python3.8/dist-packages/past/translation/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/past/utils
chmod g+s /usr/local/lib/python3.8/dist-packages/past/utils/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/past/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/past/builtins
chmod g+s /usr/local/lib/python3.8/dist-packages/past/builtins/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/past/types
chmod g+s /usr/local/lib/python3.8/dist-packages/past/types/__pycache__
chmod g+s /usr/local/lib/python3.8/dist-packages/packaging-21.3.dist-info
chmod g+s /usr/local/lib/python2.7
chmod g+s /usr/local/lib/python2.7/site-packages
chmod g+s /usr/local/lib/python2.7/dist-packages
chmod g+s /usr/local/share/fonts
chmod g+s /usr/bin/crontab
chmod g+s /usr/bin/wall
chmod g+s /usr/bin/mlock
chmod g+s /usr/bin/expiry
chmod g+s /usr/bin/at
chmod g+s /usr/bin/bsd-write
chmod g+s /usr/bin/chage
chmod g+s /usr/bin/ssh-agent
chmod g+s /usr/sbin/unix_chkpwd
chmod g+s /usr/sbin/pam_extrausers_chkpwd
chmod g+s /usr/lib/x86_64-linux-gnu/utempter/utempter
chmod g+s /var/local
chmod g+s /var/mail
chmod g+s /var/log/journal
chmod g+s /var/log/journal/d29f62c14a65486eb2d5bc189b4adbf3
chmod g+s /var/log/mysql
chmod g+s /var/log/redis
```

Well, that's good. But there are more user accounts that need the same treatment.
For example, the `mysql` user...

This is getting unworkable.
