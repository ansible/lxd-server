# Copy and rename a container

In this case I want to upgrade Nextcloud, but doing so will require upgrading
Ubuntu itself (in order to upgrade PHP, MariaDB, etc.) which will likely
break Monica and Kimai (also on the same container).

I think the simplest way around this problem is to run the Nextcloud instances
on their own container from now on, and the easiest way to achieve that is to
copy the whole existing container and then simply update the Apache redirects
only for Nextcloud.
We can remove the extraneous Kimai and Monica services from this new container
later (similarly, the Nextcloud instances can be removed from the original
container later).

Note, `lxc-copy` is part of `lxc-utils` and thus not installed.

Even with `--verbose` flag the `lxc copy` operation is completely silent, which is slightly jarring.
This copy operation worked even in the source container was running (the destination container
will be in a stopped state, though). But to ensure database integrity etc. I performed
this copy while the source container was stopped.
```
lxc stop karachi
lxc copy --verbose karachi delhi
```
By inspecting `htop` we can see that `lxc copy` is simply rsyncing the root
filesystem of the container behind the scenes:
```
rsync -a -HA --sparse --devices --delete --checksum --numeric-ids --xattrs --filter=-x security.selinux -q /var/snap/lxd/common/lxd/storage-pools/default/containers/karachi/ /var/snap/lxd/common/lxd/storage-pools/default/containers/delhi
```

The copy operation took care of the container name, but what about the IP address
and the hostname?
Exactly, the destination container cannot be started because IP address is already defined:
```
$ lxc start delhi
Error: Failed start validation for device "eth0": IP address "10.252.116.7" already defined on another NIC
Try `lxc info --show-log delhi` for more info
```
The suggested log does not show anything (since the container never started).

So what we want is for the copy operation to strip the ipv4 address field so it
can be reset for the destination container. It does this for the `volatile.eth0.hwaddr`
field already, but I wonder if we can coax it to do the same for the (non-volatile)
`ipv4.address` field?
In short, I don't think we can. After `lxc copy` we will have to set a new IPv4
address for the destination container.

```
lxc config device set delhi eth0 ipv4.address 10.252.116.13
```

Now we can successfully start the destination container.
And don't forget to restart the source container.



## Refs

+ https://manpages.ubuntu.com/manpages/xenial/en/man1/lxc-copy.1.html
+ https://discuss.linuxcontainers.org/t/lxc-copy-refresh-and-mac-address/11894/3
+ https://xpufx.com/posts/change-lxc-container-ip.md/
