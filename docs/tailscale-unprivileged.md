# Running Tailscale in an unprivileged container

First it is important to say that Tailscale has worked fine in multiple
unprivileged containers created using both the default and other profiles
from this role, and continue to do so.

While attempting to setup one container to connect to a local LAN  via another
tailnode setup as a subnet router, I discovered the following Tailscale docs page:

> Unprivileged LXC containers do not have access to the networking resource needed for Tailscale to work.
> Tailscale encapsulates its frames in UDP packets and therefore doesn’t require
> kernel modules or other privileged operations to form tunnel connections. However,
> it does need access to a `/dev/tun` device which unprivileged containers usually
> do not provide.
> https://tailscale.com/kb/1130/lxc-unprivileged

After reading other sources, and exploring the container itself, it seems to me
that no further configuration is necessary to run Tailscale in an unprivileged container.

As is, a `tun` device exists in my containers:
```
taha@kuwait:~
$ ll /dev/net/tun
crw-rw-rw- 1 nobody nogroup 10, 200 mar 23 01:06 /dev/net/tun
```

and said `tun` device is mounted:
```
taha@kuwait:~
$ cat /proc/self/mountinfo | grep tun
2391 2361 0:5 /net/tun /dev/net/tun rw,nosuid,relatime shared:183 master:2 - devtmpfs udev rw,size=16212088k,nr_inodes=4053022,mode=755,inode64
```

I want to underscore that I did not configure any of this - it is there by default
(LXC v5.0x, Ubuntu 22.04).

Still, the Tailscale log appears to complain (warnings, not errors) about lacking
permissions:
```
mar 29 00:00:10 kuwait systemd[1]: Starting Tailscale node agent...
mar 29 00:00:10 kuwait tailscaled[43629]: logtail started
mar 29 00:00:10 kuwait tailscaled[43629]: Program starting: v1.62.1-t8ee5801a3-g76428ac0b, Go 1.22.1: []string{"/usr/sbin/tailscaled", "--cleanup"}
mar 29 00:00:10 kuwait tailscaled[43629]: LogID: <logIDstring>
mar 29 00:00:10 kuwait tailscaled[43629]: logpolicy: using $STATE_DIRECTORY, "/var/lib/tailscale"
mar 29 00:00:10 kuwait tailscaled[43629]: dns: [resolved-ping=yes rc=resolved resolved=file nm=no resolv-conf-mode=stub ret=systemd-resolved]
mar 29 00:00:10 kuwait tailscaled[43629]: dns: using "systemd-resolved" mode
mar 29 00:00:10 kuwait tailscaled[43629]: creating dns cleanup: route ip+net: no such network interface
mar 29 00:00:10 kuwait tailscaled[43629]: deleting [-j ts-input] in filter/INPUT: running [/usr/sbin/iptables -t filter -D INPUT -j ts-input --wait]: exit status 2: iptables v1.8.7 (nf_tables): Chain 'ts-input' does not exist
mar 29 00:00:10 kuwait tailscaled[43629]: deleting [-j ts-forward] in filter/FORWARD: running [/usr/sbin/iptables -t filter -D FORWARD -j ts-forward --wait]: exit status 2: iptables v1.8.7 (nf_tables): Chain 'ts-forward' does not exist
mar 29 00:00:10 kuwait tailscaled[43629]: deleting [-j ts-postrouting] in nat/POSTROUTING: running [/usr/sbin/iptables -t nat -D POSTROUTING -j ts-postrouting --wait]: exit status 2: iptables v1.8.7 (nf_tables): Chain 'ts-postrouting' does not exist
mar 29 00:00:10 kuwait tailscaled[43629]: Try `iptables -h' or 'iptables --help' for more information.
mar 29 00:00:10 kuwait tailscaled[43629]: deleting [-j ts-input] in filter/INPUT: running [/usr/sbin/ip6tables -t filter -D INPUT -j ts-input --wait]: exit status 2: ip6tables v1.8.7 (nf_tables): Chain 'ts-input' does not exist
mar 29 00:00:10 kuwait tailscaled[43629]: deleting [-j ts-forward] in filter/FORWARD: running [/usr/sbin/ip6tables -t filter -D FORWARD -j ts-forward --wait]: exit status 2: ip6tables v1.8.7 (nf_tables): Chain 'ts-forward' does not exist
mar 29 00:00:10 kuwait tailscaled[43629]: Try `ip6tables -h' or 'ip6tables --help' for more information.
mar 29 00:00:10 kuwait tailscaled[43629]: [RATELIMIT] format("deleting %v in %s/%s: %v")
mar 29 00:00:10 kuwait tailscaled[43662]: wgengine.NewUserspaceEngine(tun "tailscale0") ...
mar 29 00:00:10 kuwait systemd[1]: Started Tailscale node agent.
mar 29 00:00:10 kuwait tailscaled[43662]: dns: [resolved-ping=yes rc=resolved resolved=file nm=no resolv-conf-mode=stub ret=systemd-resolved]
mar 29 00:00:10 kuwait tailscaled[43662]: dns: using "systemd-resolved" mode
mar 29 00:00:10 kuwait tailscaled[43662]: dns: using *dns.resolvedManager
mar 29 00:00:10 kuwait tailscaled[43662]: link state: interfaces.State{defaultRoute=eth0 ifs={eth0:[192.168.1.106/24 llu6]} v4=true v6=false}
mar 29 00:00:10 kuwait tailscaled[43662]: onPortUpdate(port=41641, network=udp6)
mar 29 00:00:10 kuwait tailscaled[43662]: router: using firewall mode pref
mar 29 00:00:10 kuwait tailscaled[43662]: router: default choosing iptables
mar 29 00:00:10 kuwait tailscaled[43662]: router: v6filter = true, v6nat = true
mar 29 00:00:10 kuwait tailscaled[43662]: magicsock: [warning] failed to force-set UDP read buffer size to 7340032: operation not permitted; using kernel default values (impacts throughput only)
mar 29 00:00:10 kuwait tailscaled[43662]: magicsock: [warning] failed to force-set UDP write buffer size to 7340032: operation not permitted; using kernel default values (impacts throughput only)
mar 29 00:00:10 kuwait tailscaled[43662]: onPortUpdate(port=41641, network=udp4)
mar 29 00:00:10 kuwait tailscaled[43662]: magicsock: [warning] failed to force-set UDP read buffer size to 7340032: operation not permitted; using kernel default values (impacts throughput only)
mar 29 00:00:10 kuwait tailscaled[43662]: magicsock: [warning] failed to force-set UDP write buffer size to 7340032: operation not permitted; using kernel default values (impacts throughput only)
mar 29 00:00:10 kuwait tailscaled[43662]: magicsock: disco key = d:322ce5b12870d8fd
mar 29 00:00:10 kuwait tailscaled[43662]: Creating WireGuard device...
mar 29 00:00:10 kuwait tailscaled[43662]: Bringing WireGuard device up...
mar 29 00:00:10 kuwait tailscaled[43662]: Bringing router up...
mar 29 00:00:10 kuwait tailscaled[43662]: external route: up
mar 29 00:00:10 kuwait tailscaled[43662]: Clearing router settings...
mar 29 00:00:10 kuwait tailscaled[43662]: Starting network monitor...
mar 29 00:00:10 kuwait tailscaled[43662]: Engine created.
mar 29 00:00:10 kuwait tailscaled[43662]: Start
mar 29 00:00:11 kuwait tailscaled[43662]: health("overall"): ok
mar 29 00:00:14 kuwait tailscaled[43662]: wgengine: Reconfig: configuring userspace WireGuard config (with 1/11 peers)
mar 29 00:00:14 kuwait tailscaled[43662]: magicsock: disco: node [nfYgs] d:e90d8d4702dc5060 now using 2.249.42.17:41641 mtu=1360 tx=21542189f09c
```

Note the `iptables` warnings and the multiple warnings from `magicsock`:
```
magicsock: [warning] failed to force-set UDP read buffer size to 7340032: operation not permitted; using kernel default values (impacts throughput only)
```

According to the collected wisdom of the web, an unprivileged LXC container should
be configured thus to enable TUN and allow Tailscale to work unhindered:
```
lxc.cgroup2.devices.allow: "c 10:200 rwm"
lxc.mount.entry: "/dev/net/tun dev/net/tun none bind,create=file"
```

But so far I have not been able to set these settings using either the
`community.general.lxd_profile` or `community.general.lxd_container` Ansible module
(I could not figure out if these keys were supported, and all my hit-and-miss trials
resulted in various Ansible errors).

Just from looking at the docs, it seems the `community.general.lxc_container`
could be easier to configure thanks to its `config` field. But I do not understand
the implications or differences between the `lxd` and `lxc` modules. Is it just
that only the former supports VMs?
If so, I could probably switch to using the `lxc` module for all containers
in this role without any drawbacks. But, I cannot tell at the moment.



## Links and notes

+ https://tailscale.com/kb/1130/lxc-unprivileged
+ https://tailscale.com/kb/1112/userspace-networking
+ https://www.caseyliss.com/2024/3/27/tailscale
+ https://forum.tailscale.com/t/clients-without-tailscale-accessing-via-subnet-router/5470
+ https://discuss.linuxcontainers.org/t/running-tailscale-in-lxd-vms/16543/1



### Lots of Proxmox guides

+ https://github.com/tailscale/tailscale/issues/62
+ https://github.com/idhirandar/tailscale-on-proxmox-lxc
+ https://www.armand.nz/notes/ProxMox/Running%20Tailscale%20in%20LXC%20containers
+ https://www.cloudhosting.lv/eng/faq/How-to-enable-tun-tap-inside-LXC
+ https://dustri.org/b/running-tailscale-inside-of-a-proxmox-container.html
+ https://forum.proxmox.com/threads/how-to-enable-tun-tap-in-a-lxc-container.25339
+ https://stackoverflow.com/questions/23706652/tap-tun-adapter-in-lxc-container
+ https://discuss.linuxcontainers.org/t/no-dev-net-tun-inside-lxc-container/8344/3
